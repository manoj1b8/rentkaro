//
//  CXAppConfig.swift
//  NowFloats
//
//  Created by Mahesh Y on 8/22/16.
//  Copyright © 2016 CX. All rights reserved.
//

import Foundation
import UIKit

class CXAppConfig {
    /// the singleton
    static let sharedInstance = CXAppConfig()
    
    // This prevents others from using the default '()' initializer for this class.
    fileprivate init() {
        loadConfig()
    }
    
    /// the config dictionary
    var config: NSDictionary?
    
    /**
     Load config from Config.plist
     */
    
    func loadConfig() {
        if let path = Bundle.main.path(forResource: "CXProjectConfiguration", ofType: "plist") {
            config = NSDictionary(contentsOfFile: path)
            print(config!)
        }
    }

    func getComparingOTP() -> String{
        return config!.value(forKey: "getOTP") as! String
    }
    //forgtPassword Otp
    func getForgotPasswordOtp() -> String{
        
        return config?.value(forKey: "forgotPasswordOtp") as! String
    }
    func getForgotPassordUrl() -> String {
        return config!.value(forKey: "forgotPassordMethod") as! String
    }
    func getphotoUploadUrl() -> String {
        return config!.value(forKey: "photoUpload") as! String
    }
    
    func getSignUpInUrl() -> String {
        return config!.value(forKey: "register") as! String
    }
    
    func getSignInUrl() -> String {
        return config!.value(forKey: "login") as! String
    }
    
    func getLoginOTP() -> String{
        return config!.value(forKey: "loginOTP") as! String
    }
    
    func getRegOTP() -> String {
        return config!.value(forKey: "registrationOTP") as! String
        
    }
    func getBaseUrl() -> String {
        return config!.value(forKey: "baseUrl") as! String
    }
    func paytmPayment() -> String {
        return config!.value(forKey: "paytmPayment") as! String
    }
   
    func projectName() -> String {
        return config!.value(forKey: "projectName") as! String
    }
    func getMallId() -> String {
        return config!.value(forKey: "appMallId") as! String
    }
    func contactUsNumber() -> String {
        return config!.value(forKey: "customer care") as! String
    }
    
    func getMasterUrl() -> String {
        return config!.value(forKey: "getMasters") as! String
    }
    
    func getUpdateMultipleProperties() -> String{
        return config!.value(forKey: "getUpdateMultipleProp") as! String
    }
  
    func getDeviceId() -> String {
        return  (UIDevice.current.identifierForVendor?.uuidString)!
    }
    
    func mainScreenSize() -> CGSize {
        return UIScreen.main.bounds.size
    }
    
    func getPostedJobUrl() -> String{
        return config!.value(forKey: "postJob") as! String
    }
    
    func getEmergencyVehicleRideUrl() -> String{
        return config!.value(forKey: "EmergencyVehicleRide") as! String
    }
    
    func getHospitalListUrl() -> String{
        return config!.value(forKey: "hospitalList") as! String
    }
    
    func getDriverCurrentStatus() -> String{
        return config!.value(forKey: "driverCurrentStatus") as! String
    }
    
    func cancelRideURL() -> String{
        return config!.value(forKey: "cancelRideURL") as! String
    }
    
    func ratingUrl() -> String{
        return config!.value(forKey: "ratingUrl") as! String
    }
    
    func googleAPIKey() -> String{
        return config!.value(forKey: "googleAPIKey") as! String
    }
    

    
    
    
    
    
    
  
    
    var dateFormatter = DateFormatter()
    static func resultString(input: AnyObject) -> String{
        if let value: AnyObject = input {
            var reqType : String!
            switch value {
            case let i as NSNumber:
                reqType = "\(i)"
            case let s as NSString:
                reqType = "\(s)"
            case let a as NSArray:
                reqType = "\(a.object(at: 0))"
            default:
                reqType = "Invalid Format"
            }
            return reqType
        }
        return ""
    }


    func getAppThemeColor() -> UIColor {
        let appTheamColorArr : NSArray = config!.value(forKey: "AppThemeColor") as! NSArray
        let red : Double = (appTheamColorArr.object(at: 0) as! NSString).doubleValue
        let green : Double = (appTheamColorArr.object(at: 1) as! NSString).doubleValue
        let blue : Double = (appTheamColorArr.object(at: 2) as! NSString).doubleValue
        
        return UIColor(
            red: CGFloat(red / 255.0),
            green: CGFloat(green / 255.0),
            blue: CGFloat(blue / 255.0),
            alpha: CGFloat(1.0)
        )
    }
    
    func getTheDataInDictionaryFromKey(sourceDic:NSDictionary,sourceKey:NSString) ->String{
        let keyExists = sourceDic[sourceKey] != nil
        let keyExists1 = sourceDic[sourceKey.uppercased] != nil
        let keyExists2 = sourceDic[sourceKey.lowercased] != nil
        let keyExists3 = sourceDic[sourceKey.capitalized] != nil
        if keyExists {
            // now val is not nil and the Optional has been unwrapped, so use it
            return sourceDic[sourceKey]! as! String
        }else if  keyExists1 {
            // now val is not nil and the Optional has been unwrapped, so use it
            return sourceDic[sourceKey.uppercased]! as! String
        }else if keyExists2 {
            // now val is not nil and the Optional has been unwrapped, so use it
            return sourceDic[sourceKey.lowercased]! as! String
        }else if  keyExists3{
            // now val is not nil and the Optional has been unwrapped, so use it
            return sourceDic[sourceKey.capitalized]! as! String
        }
        
        return ""
        
    }
    
    func setDeviceToken(deviceToken:String){
        UserDefaults.standard.set(deviceToken, forKey: "deviceToken")
        
    }
    
    func getDeviceToken() -> String{
        if(UserDefaults.standard.object(forKey: "deviceToken") == nil){
            return ""
        }else{
            return UserDefaults.standard.value(forKey: "deviceToken") as! String
        }
    }
    
    func convertDictionayToString(dictionary:NSDictionary) -> NSString {
        var dataString: String!
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            //CXLog.print("JSON data is \(jsonData)")
            dataString = String(data: jsonData, encoding: String.Encoding.utf8)
           // CXLog.print("Converted JSON string is \(dataString)")
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            dataString = ""
            CXLog.print(error)
        }
        return dataString as NSString
    }
    
    func convertStringToDictionary(string:String) -> NSDictionary {
        var jsonDict : NSDictionary = NSDictionary()
        let data = string.data(using: String.Encoding.utf8)
        do {
            jsonDict = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers ) as! NSDictionary            // CXDBSettings.sharedInstance.saveAllMallsInDB((jsonData.valueForKey("orgs") as? NSArray)!)
        } catch {
            CXLog.print("Error in parsing")
        }
        return jsonDict
    }
}

extension String{
    static func getDateString(dateStr:String,startDateStr:String,endDateStr:String) -> String{
        print(dateStr,startDateStr,endDateStr)
        
        let dateStr = dateStr
        let startTimeStr = startDateStr
        let endTimeStr = endDateStr
        
        let dateFormatter = DateFormatter()
        let timeFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "mm/dd/yyyy"
        timeFormatter.dateFormat = "mm/dd/yyyy HH:mm"
        
        let eventDate = dateFormatter.date(from: dateStr)
        let eventStartTime = timeFormatter.date(from: startTimeStr)
        let eventEndTime = timeFormatter.date(from: endTimeStr)
        
        
        dateFormatter.dateFormat = "dd-mm-yyyy"
        timeFormatter.dateFormat = "hh:mm a"
        
        let eDate = dateFormatter.string(from: eventDate!)
        let eSTime = timeFormatter.string(from: eventStartTime!)
        let eETime = timeFormatter.string(from: eventEndTime!)
        
        let finalString = "\(eDate) \n \(eSTime) - \(eETime)" //"19-12-2016 \n 11:30 PM - 01:30 AM"
        print(finalString)
        return finalString
    }
    
    static func genarateJsonString(dataDic:NSDictionary)->String{
        let listArray : NSMutableArray = NSMutableArray()
        listArray.add(dataDic)
        let formDict :NSMutableDictionary = NSMutableDictionary()
        formDict.setObject(listArray, forKey: "list" as NSCopying)
        var jsonData : Data = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: formDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            CXLog.print(error)
        }
        let jsonStringFormat = String(data: jsonData, encoding: String.Encoding.utf8)
        return jsonStringFormat!
    }
    
    static func gernateJsonObjec(dataDic:NSDictionary)->String{
        var jsonData : Data = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            CXLog.print(error)
        }
        let jsonStringFormat = String(data: jsonData, encoding: String.Encoding.utf8)
        return jsonStringFormat!
    }
    
    func timeAgoSinceDate(numericDates:Bool) -> String {
        let dateStr = self
        
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm MMM d',' yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let dateF = dateFormatter.date(from: dateStr)
        
        // change to a readable time format and change to local time zone
        dateFormatter.timeZone = NSTimeZone.local
        
        if let dateStr = dateF {
            let dateFinal = dateFormatter.string(from: dateStr)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm MMM d',' yyyy"
            let date = formatter.date(from: dateFinal)
            
            let calendar = NSCalendar.current
            let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
            let now = NSDate()
            let earliest = now.earlierDate(date!)
            let latest = (earliest == now as Date) ? date : now as Date
            let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest! as Date)
            
            if (components.year! >= 2) {
                return "\(components.year!) years ago"
            } else if (components.year! >= 1){
                if (numericDates){
                    return "1 year ago"
                } else {
                    return "Last year"
                }
            } else if (components.month! >= 2) {
                return "\(components.month!) months ago"
            } else if (components.month! >= 1){
                if (numericDates){
                    return "1 month ago"
                } else {
                    return "Last month"
                }
            } else if (components.weekOfYear! >= 2) {
                return "\(components.weekOfYear!) weeks ago"
            } else if (components.weekOfYear! >= 1){
                if (numericDates){
                    return "1 week ago"
                } else {
                    return "Last week"
                }
            } else if (components.day! >= 2) {
                return "\(components.day!) days ago"
            } else if (components.day! >= 1){
                if (numericDates){
                    return "1 day ago"
                } else {
                    return "Yesterday"
                }
            } else if (components.hour! >= 2) {
                return "\(components.hour!) hours ago"
            } else if (components.hour! >= 1){
                if (numericDates){
                    return "1 hour ago"
                } else {
                    return "An hour ago"
                }
            } else if (components.minute! >= 2) {
                return "\(components.minute!) minutes ago"
            } else if (components.minute! >= 1){
                if (numericDates){
                    return "Few seconds ago"
                } else {
                    return "Few seconds ago"
                }
            } else if (components.second! >= 3) {
                return "Just now"
            } else {
                return "Just now"
            }
        }
        return ""
        
    }
}




