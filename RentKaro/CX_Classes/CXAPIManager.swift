//
//  CXAPIManager.swift
//  SBeauty
//
//  Created by Raghavendra on 04/06/18.
//  Copyright © 2018 ongo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class CXAPIManager: NSObject {
    
    typealias Failure = (_ error:String)->Void
    var session_task = URLSessionTask()
    static let sharedInstance : CXAPIManager = {
        let instance = CXAPIManager()
        return instance
    }()
    
    func getJSON(path:String,parameters:[String:AnyObject],success:@escaping (Any) -> (),failure:@escaping(String) -> ()) {
        let completPath = URL(string: path)
        Alamofire.request(completPath!,method:.get).responseJSON { (response) in
            
            switch response.result{
            case .success(let data):
                if response.response?.statusCode == 200 {
                    success(data)
                }
            case .failure(let error):
                if response.response?.statusCode != nil {
                    failure(error.localizedDescription)
                }else {
                    failure(error.localizedDescription)
                }
            }
            
        }
        
    }
    
    func postJSON(path:String,Parameters:[String:AnyObject],success:@escaping(Any)->(),failure:@escaping(Any)->()) {
        
        Alamofire.request(path,method:.post,parameters:Parameters).responseJSON { (response) in
            
            switch response.result{
            case .success(let data):
                if response.response?.statusCode == 200  {
                    success(data)
                }
            case .failure(let error):
                if response.response?.statusCode != nil {
                    failure(error.localizedDescription)
                }else {
                    failure(error.localizedDescription)
                }
            }
            
        }
        
    }
    
    func prepareUrl(dic:[String:String],urlString:String) -> URL
    {
        let branchDataURL = CXAppConfig.sharedInstance.getBaseUrl()+urlString
        let url = URL(string: branchDataURL)!
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        let queryItems = dic.map{
            return URLQueryItem(name: "\($0)", value: "\($1)")
        }
        urlComponents?.queryItems = queryItems
        return urlComponents!.url!
    }
    
    //MARK:- Get Image From URL
    func getImageFromUrl(imageUrl:String) -> UIImage {
        let url = URL(string: imageUrl)
        if
            let imageURL = url  {
            let data = try? Data(contentsOf: imageURL)
            if
                let imageData = data  {
                let image = UIImage(data: imageData)
                return image!
            }
        }
        return UIImage(named: "emergencyAmb")!
    }
    
    
//    func urlSession(path:String,Parameters:[String:Any],success:@escaping(DashBoardData)->(),failure:@escaping(Error?)->()) {
//        
//        let completePath = URL(string: path)
//        let session = URLSession.shared
//        var request = URLRequest(url: completePath!)
//        request.httpMethod = "GET"
//        request.addValue("application/json", forHTTPHeaderField: "content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        do{
//            request.httpBody = try JSONSerialization.data(withJSONObject: Parameters, options: .prettyPrinted)
//        }catch{
//            print(error.localizedDescription)
//        }
//        
//        session_task = session.dataTask(with: request as URLRequest) { (data, response, error) in
//            guard error == nil else{
//                return
//            }
//            guard let data = data else{
//                return
//            }
//            do{
//                //let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
//                let httpStatus = response as? HTTPURLResponse
//                if httpStatus?.statusCode == 200 {
//                    
//                    do{
//                        let jsonDecoder = JSONDecoder()
//                        let couselorsData = try jsonDecoder.decode(DashBoardData.self, from: data)
//                        success(couselorsData)
//                    }catch let jsonErr {
//                        CXLog.print("Error decoding Json Questons", jsonErr)
//                    }
//                    
//                }else {
//                    if error != nil {
//                        failure(error!)
//                    }
//                }
//                
//            }catch{
//                failure(error)
//            }
//        }
//        session_task.resume()
//    }
    
//
//    func getDataFromServer(Url url : String ,CompletionHandler completionhandler : @escaping (DashBoardData) -> Void,failure:@escaping(String?)->())  {
//
//        // CommonMethods.instance.showLoader()
//        let completePath = URL(string: url)
//        var request = URLRequest(url: completePath!)
////        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
////        request.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        let task = URLSession.shared.dataTask(with: request as URLRequest){ (data, response, errorConnection) in
//
//            if errorConnection != nil {
//
//                OperationQueue.main.addOperation({
//
//                    failure(errorConnection?.localizedDescription )
//                })
//            }
//
//            if let httpStatus = response as? HTTPURLResponse,httpStatus.statusCode != 200 {
//
//                OperationQueue.main.addOperation({
//
//                    failure(errorConnection?.localizedDescription)
//                })
//            }
//            if let data = data {
//            do{
//                let jsonDecoder = JSONDecoder()
//                let couselorsData = try jsonDecoder.decode(DashBoardData.self, from: data)
//                completionhandler(couselorsData)
//            }catch let jsonErr {
//                CXLog.print("Error decoding Json Questons", jsonErr)
//            }
//
//            }
//
//        }
//
//        task.resume()
//
//    }
//
//
//    func sendDataToSerever( url : URL,dataString:[String:AnyObject],completionHandler:@escaping (DashBoardData) -> Void,failure:@escaping(String?)->())  {
//
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
////        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        let jsonData = try? JSONSerialization.data(withJSONObject: dataString)
//        //print(jsonData!)
//        request.httpBody = jsonData!
//        let task = URLSession.shared.dataTask(with: request as URLRequest){ (data, response, errorConnection) in
//
//            if errorConnection != nil {
//
//                OperationQueue.main.addOperation({
//
//                    failure(errorConnection?.localizedDescription)
//                })
//            }
//
//            if let httpStatus = response as? HTTPURLResponse,httpStatus.statusCode != 200 {
//
//                OperationQueue.main.addOperation({
//
//                    failure(errorConnection?.localizedDescription)
//                })
//            }
//
//            if let data = data {
//                do{
//                    let jsonDecoder = JSONDecoder()
//                    let couselorsData = try jsonDecoder.decode(DashBoardData.self, from: data)
//                    completionHandler(couselorsData)
//                }catch let jsonErr {
//
//                    CXLog.print("Error decoding Json Questons", jsonErr)
//                }
//
//            }
//
//        }
//
//        task.resume()
//
//    }
}
