
//
//  LFDataManager.swift
//  Lefoodie
//
//  Created by apple on 06/02/17.
//  Copyright © 2017 ongo. All rights reserved.
//

import UIKit
import AFNetworking
import Alamofire
import SwiftyJSON
import RealmSwift

private var sharedManager:CXDataManager! = CXDataManager()
//private var bottomTabVc : TabViewController! = TabViewController()

class CXDataManager: NSObject {
    var areaArray = NSMutableArray()

    class var sharedInstance : CXDataManager {
        return sharedManager
    }
//    func tabManager()->TabViewController{
//        return bottomTabVc
//        
//    }
    
}

extension CXDataManager{
    
    func generateBoundaryString() -> String{
        return "\(UUID().uuidString)"
    }
    // Validating OTP
    func validatingRecievedOTP(consumerEmailId:String, enteredOTP:String, completion:@escaping (_ responseDict:NSDictionary) -> Void){
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getComparingOTP(), parameters: ["ownerId":CXAppConfig.sharedInstance.getMallId() as AnyObject,"consumerEmail":consumerEmailId as AnyObject,"otp":enteredOTP as AnyObject]) { (responseDict) in
            CXLog.print(responseDict)
            completion(responseDict as NSDictionary)
            
        }
    }

    //MARK: Get Banner Images
    func getBannerImagesFromServer(completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        // http://52.74.81.232:8081/Services/getMasters?type=Stores&mallId=2
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"Stores" as AnyObject,"mallId":"2" as AnyObject]) { (responseDict) in
            completion(responseDict as NSDictionary)
        }
    }
    
    /*
    func getSignUpDetails(registerDic:NSDictionary,completion:@escaping (_ responseDict:NSDictionary,_ isGenaratedUserKey:Bool,_ emailID:String) -> Void){
        /* CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getSignUpInUrl(), parameters: ["":"","":"","":"","":""] { (responseDict) in
         
         }*/
        
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getMarshUserRegistration(),parameters: ["name":"NAUSHAD" as AnyObject,"consumerEmail":"naushad@ongo.com" as AnyObject,"LMNID":"LM/H/0060" as AnyObject,"mobile":"7893335553"as AnyObject]) { (responseDict) in
        }
    }
    */
    
    
    //MARK: Get Restaurants
    func getRestaurantsFromServer(location:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        // http://52.74.81.232:8081/Services/getMasters?type=allMalls&location=Dubai|Jumeirah
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"allMalls" as AnyObject,"location":location as AnyObject]) { (responseDict) in
            completion(responseDict as NSDictionary)
        }
    }
   

    //MARK: Get Cuisine Types
    func getCuisinesForSelectedRestaurant(restaurantId:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        // http://52.74.81.232:8081/Services/getMasters?type=PSubCategories&mallId=36
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"PSubCategories" as AnyObject,"mallId":restaurantId as AnyObject]) { (responseDict) in
            completion(responseDict as NSDictionary)
        }
    }
    
    //MARK: Get Meal Types
    func getMealsForSelectedRestaurant(restaurantId:String,cuisineId:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        // http://52.74.81.232:8081/Services/getMasters?type=P3rdLevelCategories&mallId=36&refId=810&refTypeProperty=SubCategory
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"P3rdLevelCategories" as AnyObject,"mallId":restaurantId as AnyObject,"refId":cuisineId as AnyObject,"refTypeProperty":"SubCategory" as AnyObject]) { (responseDict) in
            completion(responseDict as NSDictionary)
        }
    }
    
    //MARK: Get Packages
    func getPackagesForSelectedRestaurant(restaurantId:String,mealId:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        // http://52.74.81.232:8081/Services/getMasters?type=Packages&mallId=36&refId=1346&refTypeProperty=Meal
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"Packages" as AnyObject,"mallId":restaurantId as AnyObject,"refId":mealId as AnyObject,"refTypeProperty":"Meal" as AnyObject]) { (responseDict) in
            completion(responseDict as NSDictionary)
        }
    }
 /*
    //MARK: Image Upload
    func imageUpload( imageData:Data,completion:@escaping (_ responseDict:NSDictionary) -> Void){
        let mutableRequest : AFHTTPRequestSerializer = AFHTTPRequestSerializer()
        let request1 : NSMutableURLRequest =    mutableRequest.multipartFormRequest(withMethod: "POST", urlString: CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getphotoUploadUrl(), parameters: ["refFileName":generateBoundaryString()], constructingBodyWith: { (formatData:AFMultipartFormData) in
            formatData.appendPart(withFileData: imageData, name: "srcFile", fileName: "uploadedFile.jpg", mimeType: "image/jpeg")
        }, error: nil)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request1 as URLRequest) {
            (
            data, response, error) in
            if (data != nil){
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                let myDic = dataString?.convertStringToDictionary()  //self.convertStringToDictionary(dataString! as String)
                completion(myDic!)
            }else{
                completion(NSDictionary())
            }
        }
        task.resume()
    }
    */
    //MARK : Update Multiple properties
    func getUpdateMultipleProperties(jobId:String,jsonString:String,completion:@escaping (_ responseDict:NSDictionary) -> Void){
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getPostedJobUrl(), parameters: ["type":"Opt Out"as AnyObject,"jobId":jobId as AnyObject, "json":jsonString as AnyObject,"ownerId":CXAppConfig.sharedInstance.getMallId() as AnyObject,"category":"Services" as AnyObject,"dt":"CAMPAIGNS" as AnyObject]) { (responceDic) in
            completion(responceDic as NSDictionary)
        }
    }
   
    //MARK : Change Password
/*
    func  changePassword(email:String,currentPsw:String,newPsw:String,confirmPsw:String,completion:@escaping (_ responseDict:NSDictionary) -> Void){
        
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getChangePasswordUrl(), parameters: ["orgId":CXAppConfig.sharedInstance.getAppMallID() as AnyObject,"dt":"DEVICES" as AnyObject,"cPassword":currentPsw as AnyObject,"nPassword":newPsw as AnyObject,"nrPassword":confirmPsw as AnyObject,"email":email as AnyObject]) { (responseDict) in
            completion(responseDict)
        }
    }
*/
    //MARK : Update Multiple properties
    


    //MARK : UPDATE PROFILE
    /*
    func profileUpdate(_ email:String,address:String,firstName:String,lastName:String,mobileNumber:String,image:String,completion:@escaping (_ responseDict:NSDictionary)-> Void){
        
        // CXDataService.sharedInstance.imageUpload(UIImageJPEGRepresentation(image, 0.5)!) { (imageFileUrl) in
        
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getupdateProfileUrl(), parameters: ["orgId":CXAppConfig.sharedInstance.getMallId() as AnyObject,"email":email as AnyObject,"dt":"DEVICES" as AnyObject,"address":address as AnyObject,"firstName":firstName as AnyObject,"lastName":lastName as AnyObject,"mobileNo":mobileNumber as AnyObject,"userBannerPath":image as AnyObject,"userImagePath":image as AnyObject]) { (responseDict) in
            completion(responseDict)
        }
        
        //}
        //   NSString* urlString = [NSString stringWithFormat:@"%@orgId=%@&email=%@&dt=DEVICES&firstName=%@&lastName=%@&address=%@&mobileNo=%@&city=%@&state=%@&country=%@&userImagePath=%@&userBannerPath=%@",UpdateProfile_URL,mallId,dict[@"emailId"], dict[@"firstName"],dict[@"lastName"],dict[@"address"],dict[@"mobile"],dict[@"city"],dict[@"state"],dict[@"country"], dict[@"userImagePath"], dict[@"userBannerPath"]];
        
    }
  */
    // get update userdara
    
    //MARK: Subsriptions
    // http://localhost:8081/MobileAPIs/createOrUpdateSubscription?mallId=36&consumerEmail=dosaplaza@ongo.com&jsonString={"list":[{"Name":"satya","Address":"Madhapur,Madhapur Police Station","ValidFrom":"chandra","Mobile":"8121415769","Extended Date":"05/19/2017","Exception Days":"Sunday","ValidFrom":"05/10/2017","ValidTill":"05/15/2017","Email":"dosaplaza@ongo.com","Cancel Dates":"","ProductId":"859","Meal Choice":"Dinner"},{"Name":"satya","Address":"Madhapur,Madhapur Police Station","ValidFrom":"chandra","Mobile":"8121415769","Extended Date":"05/19/2017","Exception Days":"Sunday","ValidFrom":"05/10/2017","ValidTill":"05/15/2017","Email":"dosaplaza@ongo.com","Cancel Dates":"","ProductId":"859","Meal Choice":"Dinner"}]}
    /*
     Name
     ValidFrom = from date
     ExtendedDate = “not at imple”
     ValidTill  = toDate
     Email = “”
     ExceptionDays = “date,date”
     ProductId = id
     MealChoice = “lunch/dinner”
     FoodChoice = veg/non
     [17/05/17, 5:24:53 PM] Mahesh: Address
     [17/05/17, 5:24:58 PM] Mahesh: Mobile
     MealPlanDays = “6/10”
     Quantity
     MRP
     */
    
   /* func subscriptionOrOrderPost(_ consumerEmail: String,name:String,address:String,mobile:String,restrarentMall:String,validFrom:String,extendedDate:String,validTill:String,exceptionDays:String,productId:String,mealChoice:String,foodChoice:String,mealPlanDays:String,quantity:String,mrp:String,giftMeal:String,completion:@escaping (_ isDataSaved:Bool) -> Void){
        // here restrarent mall Id = mallid
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getPlaceOrderUrl(), parameters: ["mallId":restrarentMall as AnyObject,"consumerEmail":consumerEmail as AnyObject,"jsonString":self.checkOutOder(name, validFrom: validFrom, extendedDate: extendedDate, validTill: validTill, email: consumerEmail, exceptionDays: exceptionDays, productId: productId, mealChoice: mealChoice, foodChoice: foodChoice, address: address, mobile: mobile, mealPlanDays: mealPlanDays, quantity: quantity, mrp: mrp, restrarentMallID1: restrarentMall,giftMealBox: giftMeal) as AnyObject]) { (responseDiict) in
            completion(true)
            let string = responseDiict.value(forKeyPath: "status") as! String
            if (string.contains("1")){
                
                CXLog.print("Succesfully")
            }else{
            
             CXLog.print("Not Succesfully")
            }
            
            
        }
    }*/
    
    
   /* func checkOutOder(_ name:String,validFrom:String,extendedDate:String,validTill:String,email:String,exceptionDays:String,productId:String,mealChoice:String,foodChoice:String,address:String,mobile:String,mealPlanDays:String,quantity:String,mrp:String,restrarentMallID1:String,giftMealBox:String)->String{
        
        let listArray : NSMutableArray = NSMutableArray()
        let realm = try! Realm()
        let cartList = realm.objects(CartData.self)
        for cart in cartList{
            //CXLog.print(cart)
            // CXLog.print(cart.productId)
            
            let order: NSMutableDictionary = NSMutableDictionary()
            order.setValue(cart.name, forKey: "Name")
            order.setValue(cart.validFrom, forKey: "ValidFrom")
            order.setValue(cart.extendedDate, forKey: "ExtendedDate")
            order.setValue(cart.validTill, forKey: "ValidTill")
            order.setValue(email, forKey: "Email")
            order.setValue(cart.exceptionDays, forKey: "ExceptionDays")
            order.setValue(cart.productId, forKey: "ProductId")
            order.setValue(cart.mealChoice, forKey: "MealChoice")
            order.setValue(cart.foodChoice, forKey: "FoodChoice")
            order.setValue(address, forKey: "Address")
            order.setValue(mobile, forKey: "Mobile")
            order.setValue(cart.mealPlanDays, forKey: "MealPlanDays")
            order.setValue(cart.quantity, forKey: "Quantity")
            order.setValue(cart.mrp, forKey: "MRP")
            order.setValue(giftMealBox, forKey: "GiftAMealBox")
            order.setValue(cart.bufferedDays, forKey: "BufferedDays")
            order.setValue(cart.foodChoiceDays, forKey: "FoodChoiceDays")
            listArray.add(order)
        }
        let cartJsonDict :NSMutableDictionary = NSMutableDictionary()
        cartJsonDict.setObject(listArray, forKey: "list" as NSCopying)
        var jsonData : Data = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: cartJsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            print(error)
        }
        let jsonStringFormat = String(data: jsonData, encoding: String.Encoding.utf8)
        //CXLog.print("order dic \(String(describing: jsonStringFormat!))")
        return jsonStringFormat!
    }*/
    
    //MARK: Get Areas
   /* func getAreasFromServer(cityName:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        // http://52.74.81.232:8081/Services/getMasters?type=Dubai_areas&mallId=2
        // http://52.74.81.232:8081/Services/getMasters?type=Sharjah&mallId=2
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":cityName as AnyObject,"mallId":CXAppConfig.sharedInstance.getAppMallID as AnyObject]) { (responseDict) in
            completion(responseDict)
//           self.areaArray = responseDict.value(forKeyPath: "jobs") as! NSMutableArray
           // self.areaArray.add(responseDict)
        }
    }
    
    //MARK: payment integration -- CCAvenu
    /*http://52.74.81.232/CCAvenue/ccapayments?userId=2&amount=1&billing_name=vinodha&billing_address=HYD&billing_city=HYD&billing_state=TS&billing_zip=500081&billing_country=India&billing_tel=02002020245678&billing_email=vinodhapudari@gmail.com
     */
    func ccAvenuePayment(userId:String,amount:String,billingName:String,billingAddress:String,billingCity:String,billingState:String,billingZip:String,billingCountry:String,billingMobileNumber:String,billingEmail:String){
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getTestPaymentGatewayUrl(), parameters: ["userId":userId as AnyObject,"amount":amount as AnyObject,"billing_name":billingName as AnyObject,"billing_address":billingAddress as AnyObject,"billing_city":billingCity as AnyObject,"billing_state":billingState as AnyObject,"billing_zip":billingZip as AnyObject,"billing_country":billingCountry as AnyObject,"billing_tel":billingMobileNumber as AnyObject,"billing_email":billingEmail as AnyObject]) { (responseDict) in
            //CXLog.print(responseDict)
        }
        
        
        
    }*/
    
    //MARK: Get order history data 
    
   /* func getOrderHistoryData(consumerId:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
        //http://52.74.81.232:8081/Services/getMasters?type=User%20Subscriptions&myJobs=true&consumerId=57
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"User Subscriptions" as AnyObject,"myJobs":true as AnyObject,"consumerId":consumerId as AnyObject]) { (responseDict) in
            completion(responseDict)
        }
    }*/
    //MARK: Get order Tracking data
    func getOrderTrackingData(consumerId:String,singpleProp:String,completion:@escaping (_ responseDict:NSDictionary) -> Void)
    {
//http://52.74.81.232:8081/Services/getMasters?type=PlaceOrder&myJobs=true&consumerId=57&singpleProp=OrderDate-05/26/2017
        
        let url = CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getMasterUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(url, parameters: ["type":"PlaceOrder" as AnyObject,"myJobs":true as AnyObject,"consumerId":consumerId as AnyObject,"singpleProp":singpleProp as AnyObject]) { (responseDict) in
            completion(responseDict as NSDictionary)

        }
    }
    
    //MARK: Forgot Password
  /*
    func  forgotPassword(_ email:String,completion:@escaping (_ responseDict:NSDictionary) -> Void){
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getForgotPassordUrl(), parameters: ["orgId":CXAppConfig.sharedInstance.getAppMallID() as AnyObject,"email":email as AnyObject,"dt":"DEVICES" as AnyObject]) { (responseDict) in
            completion(responseDict)
        }
    }
*/
    
}

extension NSString{
    func convertStringToDictionary() -> NSDictionary {
        var jsonDict : NSDictionary = NSDictionary()
        let data = self.data(using: String.Encoding.utf8.rawValue)
        do {
            jsonDict = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers ) as! NSDictionary            // CXDBSettings.sharedInstance.saveAllMallsInDB((jsonData.valueForKey("orgs") as? NSArray)!)
        } catch {
            CXLog.print("Error in parsing")
        }
        return jsonDict
    }
}
