//
//  CXDataService.swift
//  FanTicket
//
//  Created by apple on 09/12/16.
//  Copyright © 2016 ongo. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import ActionSheetPicker_3_0
import AFNetworking

private var _SingletonSharedInstance:CXDataService! = CXDataService()

let delegate : AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!

class CXDataService: NSObject {
    let timeOutInterval : Int = 60
    
    var progressSmall : MBProgressHUD!
    var isRemoveGestrure = false
    var isFromRestaurant = false
    
    class var sharedInstance : CXDataService {
        return _SingletonSharedInstance
    }
    
  
    func categoryPageDataFromServer(path:String,params:[String:AnyObject],success:@escaping(Any)->(),failure:@escaping(String)->()) {
        
        CXAPIManager.sharedInstance.getJSON(path: path, parameters: params, success: { (response) in
            print(response)
            success(response)
        }) { (error) in
            failure(error)
        }
        
    }
    
    func SendDataFromServer(path:String,params:[String:AnyObject],success:@escaping(Any)->(),failure:@escaping(Any)->()) {
        
        CXAPIManager.sharedInstance.postJSON(path: path, Parameters: params, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
        
    }
    
    ///----@@@@------- Common Generic medhod for all trype of data -------------@@@@-------------
    ////http://storeongo.com:8081/Services/getMasters?type=Blood%20Banks&mallId=23096
    func getDataFromServerGeneric<T: Decodable>(urlString: String, parameters:[String:String],completion:@escaping (_ getDashBoardData:T)->()){
        
        if !Connectivity.isConnectedToInternet() {
            //            Connectivity.showAlert()
            return
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        CXLog.print("Base Url==\(url)")
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data,_ ,_ ) in
            guard let data = data else {
                return
            }
            do{
                let jsonDecoder = JSONDecoder()
                let couselorsData = try jsonDecoder.decode(T.self, from: data)
                completion(couselorsData)
            }catch let jsonErr {
                CXLog.print("Error decoding Json Questons", jsonErr)
            }
        }
        task.resume()
    }
    func generateBoundaryString() -> String
    {
        return "\(UUID().uuidString)"
    }
    
    func convertStringToDictionary(_ string:String) -> NSDictionary {
        var jsonDict : NSDictionary = NSDictionary()
        let data = string.data(using: String.Encoding.utf8)
        do {
            jsonDict = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers ) as! NSDictionary            // CXDBSettings.sharedInstance.saveAllMallsInDB((jsonData.valueForKey("orgs") as? NSArray)!)
        } catch {
        }
        return jsonDict
    }
    
    open func imageUpload(_ imageData:Data,completion:@escaping (_ Response:NSDictionary) -> Void){
        if !Connectivity.isConnectedToInternet() {
            CXLog.print("Yes! internet is available.")
            self.showAlertView(status: 0)
            return
            // do some tasks..
        }
        
        let mutableRequest : AFHTTPRequestSerializer = AFHTTPRequestSerializer()
        let request1 : NSMutableURLRequest =    mutableRequest.multipartFormRequest(withMethod: "POST", urlString: CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getphotoUploadUrl(), parameters: ["refFileName": self.generateBoundaryString()], constructingBodyWith: { (formatData:AFMultipartFormData) in
            formatData.appendPart(withFileData: imageData, name: "srcFile", fileName: "uploadedFile.jpg", mimeType: "image/jpeg")
        }, error: nil)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request1 as URLRequest) {
            (
            data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response  , error == nil else {
                return
            }
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let myDic = self.convertStringToDictionary(dataString! as String)
            completion(myDic)
            
        }
        
        task.resume()
        
    }

    class Connectivity {
        class func isConnectedToInternet() ->Bool {
            return NetworkReachabilityManager()!.isReachable
        }
        
        class func showAlert(title:String,msg:String){
            DispatchQueue.main.async{
                guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                delegate.window?.rootViewController?.hideLoader()
                delegate.window?.rootViewController?.showAlert(withTitle: title, andMessage: msg)
            }
        }
    }
    
    //MARK:- Get the data from API response in dictionary
    open func synchDataToServerAndServerToMoblile(_ urlstring:String, parameters:[String: AnyObject],completion:@escaping (_ responseDict:[String:AnyObject]) -> Void) {
        CXLog.print(urlstring)
        CXLog.print(parameters)
        if !Connectivity.isConnectedToInternet() {
            Connectivity.showAlert(title: AFWrapper.applicationName, msg: "No Internet Connection")
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        Alamofire.request(urlstring,method:.post,parameters:parameters).responseJSON { (response) in
            switch response.result{
            case .success(let value):
                CXLog.print(value)
                if let result = response.value {
                    completion(result as! [String : AnyObject])
                }
            case .failure(let error):
                if error._code == NSURLErrorTimedOut || error._code == NSURLErrorCancelled {
                    //timeout here
                    
                }
                else if error._code == NSURLErrorNotConnectedToInternet || error._code == NSURLErrorNetworkConnectionLost {
                    
                    
                }
            }
        }
    }
      //MARK:- Get the data from API response in data
    func getDataFromServer(path:String,parameters:[String:AnyObject],success: @escaping (Data) -> (),failure:@escaping(String) -> ()) {
        CXLog.print(parameters)
        if !Connectivity.isConnectedToInternet() {
            Connectivity.showAlert(title: AFWrapper.applicationName, msg: "No Internet Connection")
            UIApplication.shared.endIgnoringInteractionEvents()
            failure("The internet connection appears to be offline")
            return
        }
        if  let completPath = URL(string: path) {
            Alamofire.request(completPath,method:.post,parameters:parameters).responseJSON { (response) in
                switch response.result{
                case .success(let value):
                    CXLog.print(value)
                    if let result = response.data {
                        success(result)
                    }
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut || error._code == NSURLErrorCancelled {
                        //timeout here
                        failure("The request timed out")
                        
                    }
                    else if error._code == NSURLErrorNotConnectedToInternet || error._code == NSURLErrorNetworkConnectionLost {
                        failure("The internet connection appears to be offline")
                        
                    }
                }
            }
        }
    }
    
    func sendTheNotificaton(notificationMesDic:[String:AnyObject]){
        
        let headers: HTTPHeaders = [
            "Authorization": "key=AIzaSyDdRqvGVzUTMb2UKGrZPpNezsm6lApblRM",
            "Accept": "application/json"
        ]
        
        CXLog.print(notificationMesDic)
        Alamofire.request("https://fcm.googleapis.com/fcm/send", method: .post, parameters: notificationMesDic as? Parameters, encoding: JSONEncoding.default, headers: headers).responseString { response in
            print(response.result)
            switch response.result {
            case .success: break
            default :
                break
            }
        }
    }

    //MARK:- ActionSheetPicker Method
    func showPicker(title:String,rows:[[Any]], initialSelection:
        [Int],completionPicking:@escaping (_ value:[String])->Void,controller :UIViewController) {
        
        ActionSheetMultipleStringPicker.show(withTitle: title, rows: rows, initialSelection: initialSelection, doneBlock: {
            picker, indexes, values in
            if let key = values! as? [String] {
                completionPicking(key)
                
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: controller.view)
        
    }
    
    //MARK:- ActionSheetPicker Method
    func showPickerMethod(title:String,rows:[[Any]], initialSelection:
        [Int],completionPicking:@escaping (_ value:[String],_ index:[Any])->Void,controller :UIViewController) {
        
        ActionSheetMultipleStringPicker.show(withTitle: title, rows: rows, initialSelection: initialSelection, doneBlock: {
            picker, indexes, values in
            if let key = values! as? [String] {
                let index = indexes
                completionPicking(key,index ?? [])
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: controller.view)
        
    }
    
    //MARK:- Select DatePicker Method
    func selectDatePicker(completionPicking:@escaping (_ value:String)->Void,controller:UIViewController) {
        var dateString = String()
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        
        let date = calendar.date(from: minDateComponent)
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: date, doneBlock: {
            picker, value, index in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let birthDateText = dateFormatter.string(from: value as! Date)
            completionPicking(birthDateText)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin:controller.view)
        let _: TimeInterval = 0;
        var components = DateComponents()
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        components.month = 1
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        datePicker?.minimumDate = minDate
        datePicker?.maximumDate = maxDate
        datePicker?.show()
    }
    
    //MARK:- Select TimePicker Method
    func selectTimePicker(completionPicking:@escaping (_ value:String)->Void,controller:UIViewController) {
        var dateString = String()
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.hour,.minute], from: Date())
        let date = calendar.date(from: minDateComponent)
        let datePicker = ActionSheetDatePicker(title: "Select Time", datePickerMode: UIDatePicker.Mode.time, selectedDate: date, doneBlock: {
            picker, value, index in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let birthDateText = dateFormatter.string(from: value as! Date)
            completionPicking(birthDateText)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin:controller.view)
        let _: TimeInterval = 0;
        var components = DateComponents()
        
        components.hour = 4
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        datePicker?.minimumDate = Date()
        datePicker?.maximumDate = maxDate
        datePicker?.show()
    }
}

extension CXDataService{
    
    func showAlertView(status:Int) {
        //self.hideLoader()
        let alert = UIAlertController(title:"Network Error!!!", message:"Please bear with us.Thank You!!!", preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if status == 1 {
                
            }else{
                
            }
        }
        alert.addAction(okAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(message:String,viewController:UIViewController){
        let alert = UIAlertController.init(title: AFWrapper.applicationName, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "Ok", style: .default) { (okAction) in
            
        }
        alert.addAction(okAction)
        viewController.present(alert, animated: true, completion: nil)
    }
}

extension CXDataService {
    
    func uploadDoc(parametersDict:[String:Any],imageDataDic:[String:Data],uploadUrl:String,completion:@escaping (_ isSuccess:Bool,_ responseDict:[String:AnyObject]) -> Void)
    {
        //NOTE: For upload attachment user below url
        //CXAppConfig.sharedInstance.getBaseUrl() + "Services/uploadMultipleFiles?"
        //for single image with json user posted job url
        //CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getPlaceOrderUrl()
        CXLog.print(uploadUrl)
        CXLog.print(parametersDict)
        CXLog.print(imageDataDic)
        if !Connectivity.isConnectedToInternet() {
            Connectivity.showAlert(title: AFWrapper.applicationName, msg: "No Internet Connection")
            UIApplication.shared.endIgnoringInteractionEvents()
            completion(false,[String:AnyObject]())
            return
        }
        //Attachments
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (theKey, theValue) in imageDataDic {
                multipartFormData.append(theValue, withName: "srcFile",fileName: theKey, mimeType: "image/jpg")
                CXLog.print(theKey)
            }
            //  multipartFormData.append(imgData, withName: "srcFile",fileName: "addressProof.jpg", mimeType: "image/jpg")
            for (theKey, theValue) in parametersDict {
                multipartFormData.append((theValue as! String).data(using: String.Encoding.utf8)!, withName: theKey )
            }
        },
                         to:uploadUrl,
                         method:.post)
        { (result) in
            switch result {
            case .success(let upload,_, _):
                
                upload.uploadProgress(closure: { (progress) in
                    
                    CXLog.print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
                
                upload.responseJSON { response in
                    CXLog.print(response)
                    completion(true,response.result.value as! [String:AnyObject])
                }
                
            case .failure(let encodingError):
                CXLog.print(encodingError)
                completion(false,[String:AnyObject]())
            }
        }
    }
}




