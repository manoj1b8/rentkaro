//
//  CXDataSaveManager.swift
//  Lefoodie
//
//  Created by apple on 15/02/17.
//  Copyright © 2017 ongo. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

private var savedManager:CXDataSaveManager! = CXDataSaveManager()

class CXDataSaveManager: NSObject , URLSessionDownloadDelegate, UIDocumentInteractionControllerDelegate{
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    
    class var sharedInstance : CXDataSaveManager {
        return savedManager
    }
   
    func saveTheUserDetails(userDataDic:JSON){
        
        // LFMyProfile
        let relamInstance = try! Realm()
        // Query Realm for userProfile contains name
        let userData = relamInstance.objects(UserAccountDB.self).filter("userId=='\(userDataDic["id"].stringValue)'")
        
        if userData.count == 0{
            
        }
        
    }
    
    
    func updateTheUserDetails(userDataDic:JSON){
        
        // LFMyProfile
        let relamInstance = try! Realm()
        // Query Realm for userProfile contains name
        let userData = relamInstance.objects(UserAccountDB.self).filter("userId=='\(userDataDic["id"].stringValue)'")
        
        if userData.count == 0 {
            try! relamInstance.write({
                if let myProfileData = userData.first{
                    
                }
            })
        }

    }
    
    func getTheUserProfileFromDB() -> UserAccountDB{
        let realm = try! Realm()
        let profileList = realm.objects(UserAccountDB.self)
        if profileList.count != 0 {
            let profile = realm.objects(UserAccountDB.self).first
            return profile!
        }else{
            return UserAccountDB()
        }
    }
 
    func pdfDownLoad(pdfUrl:String){
        
        let url = URL(string: pdfUrl)!
        CXLog.print(url)
        downloadTask = backgroundSession.downloadTask(with: url)
        downloadTask.resume()
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/file.pdf"))
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
           // showFileWithPath(path: destinationURLForFile.path)
        }
        else{
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                // show file
               // showFileWithPath(path: destinationURLForFile.path)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    // 2
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
      ///  progressView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
    }
    
    //MARK: URLSessionTaskDelegate
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?){
        downloadTask = nil
       // progressView.setProgress(0.0, animated: true)
        if (error != nil) {
            print(error!.localizedDescription)
        }else{
            print("The task finished transferring data successfully")
        }
    }
    
    //MARK: UIDocumentInteractionControllerDelegate
    
}
/*
extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
       // let yourDate: Date? = formatter.date(from: myString)

        return dateFormatter.string(from: self)
    }
    
    func isBetweeen(date date1: Date, andDate date2: Date) -> Bool {
        return date1.compare(self) == self.compare(date2)
    }
    
}
*/
