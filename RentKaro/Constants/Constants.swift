//
//  Constants.swift
//  iOSOngoTemplate
//
//  Created by Mahesh on 23/10/18.
//  Copyright © 2018 ongoframework. All rights reserved.
//

import UIKit


struct Storyboards {
    static let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    static let dashBoardStoryboard = UIStoryboard.init(name: "DashBoard", bundle: nil)
    static let myOrdersStoryboard = UIStoryboard.init(name: "MyOrders", bundle: nil)
}

struct ReusableCellIdentifiers {
    static let homeCollectionViewCell = "HomeCollectionViewCell"
   
}

struct NotificationName {
    static let sample = ""
}

struct ViewControllerIdentifiers {
    static let signInViewController = "SignInViewController"
    
}

struct ResponseStatusCode {
}

struct ViewFrames {
    static let mainScreenFrame = UIScreen.main.bounds
}

