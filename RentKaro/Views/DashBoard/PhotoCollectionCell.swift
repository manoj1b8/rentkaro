//
//  PhotoCollectionCell.swift
//  RentKaro
//
//  Created by Teja's Macbook on 17/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {

    @IBOutlet weak var uploadedImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        uploadedImageView.layer.cornerRadius = 8.0
        uploadedImageView.layer.borderColor = UIColor(red: 112.0/255.0, green: 112.0/255.0, blue: 112.0/255.0, alpha: 1.0).cgColor
        uploadedImageView.layer.borderWidth = 0.5
    }

}
