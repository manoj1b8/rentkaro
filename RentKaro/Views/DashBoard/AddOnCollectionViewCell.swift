//
//  AddOnCollectionViewCell.swift
//  RentKaro
//
//  Created by Apple on 16/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AddOnCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var addOnImageView: UIImageView!
    @IBOutlet weak var addOnLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.layer.masksToBounds = true
        self.bgView.layer.cornerRadius = 5
    }

}
