//
//  FavotiteTableViewCell.swift
//  SnehaChicken
//
//  Created by Apple on 30/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FavotiteTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var actualPriceView: UIView!
    @IBOutlet weak var actualPrice: UILabel!
    @IBOutlet weak var priceAfterDiscount: UILabel!
    @IBOutlet weak var productWeightLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingsCountLabel: UILabel!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var productQuantityChangeView: UIView!
    @IBOutlet weak var productQuantityLabel: UILabel!
    var productCount = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addToCartBtn.layer.cornerRadius = 16
        self.productQuantityChangeView.isHidden = true
    }
    
    @IBAction func addBtnAction(_ sender: Any) {
        self.productCount += 1
        self.addToCartBtn.isHidden = true
        self.productQuantityChangeView.isHidden = false
        self.productQuantityLabel.text = "\(self.productCount)"
    }
    
    
    @IBAction func descendProductQuantityBtnAction(_ sender: Any) {
        self.productCount -= 1
        if self.productCount == 0{
            self.productQuantityChangeView.isHidden = true
            self.addToCartBtn.isHidden = false
        }else{
            self.productQuantityChangeView.isHidden = false
            self.addToCartBtn.isHidden = true
            self.productQuantityLabel.text = "\(self.productCount)"
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
