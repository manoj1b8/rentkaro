//
//  MyVehicleTableViewCell.swift
//  RentKaro
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class MyVehicleTableViewCell: UITableViewCell {

    @IBOutlet weak var carDetailsLabel: UILabel!
    @IBOutlet weak var licenseNoLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var showStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.showStatusLabel.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
