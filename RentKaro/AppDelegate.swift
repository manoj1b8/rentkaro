//
//  AppDelegate.swift
//  RentKaro
//
//  Created by Apple on 12/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        GMSServices.provideAPIKey(CXAppConfig.sharedInstance.googleAPIKey())
        GMSPlacesClient.provideAPIKey(CXAppConfig.sharedInstance.googleAPIKey())
        //self.setUpSidePanel()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "RentKaro")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension  AppDelegate {
    
    //MARK:- Navigate To Login
    func navigateToLogin() {
        guard let loginVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainLoginRegVC") as? MainLoginRegVC else{
            return
        }
        let navigationVC = UINavigationController.init(rootViewController: loginVC)
        navigationVC.navigationBar.isHidden = true
        self.window?.rootViewController = navigationVC
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func setUpSidePanel()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = storyBoard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        let nav = UINavigationController(rootViewController: homeVC)
        nav.navigationBar.barTintColor = UIColor.init(named: "appTheme") ?? UIColor.purple
        nav.navigationBar.tintColor = UIColor.white
        
        guard let menuVC = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController else{
            return
        }
        let revealVC = SWRevealViewController()
        revealVC.rearViewController = menuVC
        revealVC.frontViewController = nav
        self.window?.rootViewController = revealVC
        self.window?.makeKeyAndVisible()
    }
    
    func logout()
    {
        for view in (self.window?.subviews)!{
            view.removeFromSuperview()
        }
        let myProfile = UserDataManager.sharedInstance.getTheUserProfileFromDB()
        self.deleteUserDataFromRealm()
        self.navigateToLogin()
    }
    
    func deleteUserDataFromRealm(){
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
            UserDefaults.standard.removeObject(forKey:"myMail")
        }
    }
}

