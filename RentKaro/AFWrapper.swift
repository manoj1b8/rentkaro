//
//  AFWrapper.swift
//  RaviInfotech
//
//  Created by Rama kuppa on 2/16/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class AFWrapper {
    static let mainURL = ""
    static let applicationName = CXAppConfig.sharedInstance.projectName()
    static let appDelegate = UIApplication.shared.delegate as? AppDelegate
}

func RIAddBadgeCount()
{
    if UserDefaults.standard.object(forKey: "badgeCount") != nil {
        var count:NSInteger = NSInteger(UserDefaults.standard.object(forKey: "badgeCount") as! String)!
        count = count + 1
        UserDefaults.standard.removeObject(forKey: "badgeCount")
        UserDefaults.standard.synchronize()
        let str:String = String(format:"%lu",count)
        UserDefaults.standard.set(str, forKey: "badgeCount")
        UserDefaults.standard.synchronize()
    }else
    {
        UserDefaults.standard.set("1", forKey: "badgeCount")
        UserDefaults.standard.synchronize()
    }
}

func RIBadgeCount() -> String
{
    if UserDefaults.standard.object(forKey: "badgeCount") != nil
    {
        let badge = UserDefaults.standard.object(forKey: "badgeCount") as! String
        return badge
    }else
    {
        return ""
    }
}

func RISetBadgeCount(badge:NSInteger)
{
    UserDefaults.standard.removeObject(forKey: "badgeCount")
    UserDefaults.standard.synchronize()
    if badge != 0 {
        let str:String = String(format:"%lu",badge)
        UserDefaults.standard.set(str, forKey: "badgeCount")
        UserDefaults.standard.synchronize()
    }
}


