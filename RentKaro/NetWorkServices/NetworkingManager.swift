//
//  NetworkingManager.swift
//  iOSOngoTemplate
//
//  Created by Mahesh on 23/10/18.
//  Copyright © 2018 ongoframework. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AFNetworking

//Good to use final keyword to indicate our singleton cannot be modified or subclassed
class NetworkingManager {
    static let instance = NetworkingManager()
    // Good to use private keyword to prevent other objects from creating their own instances
    private init() {}
    
    func getDataFromServer(path:String,parameters:[String:AnyObject],success: @escaping (Data) -> (),failure:@escaping(String) -> ()) {
        CXLog.print(parameters)
        if !Connectivity.isConnectedToInternet() {
            Connectivity.showAlert(title: "SRIS", msg: "No Internet Connection")
            UIApplication.shared.endIgnoringInteractionEvents()
            failure("The internet connection appears to be offline")
            return
        }
        if  let completPath = URL(string: path) {
            Alamofire.request(completPath,method:.post,parameters:parameters).responseJSON { (response) in
                switch response.result{
                case .success(let value):
                    CXLog.print(value)
                    if let result = response.data {
                        success(result)
                    }
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut || error._code == NSURLErrorCancelled {
                        //timeout here
                        failure("The request timed out")
                        
                    }
                    else if error._code == NSURLErrorNotConnectedToInternet || error._code == NSURLErrorNetworkConnectionLost {
                        failure("The internet connection appears to be offline")
                    
                    }
                }
            }
        }
    }
    
    
    /*func SendDataFromServer(path:String,params:[String:AnyObject],success:@escaping(Any)->(),failure:@escaping(Any)->()) {
        
        postJSON(path: path, Parameters: params, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
        
    } */
    
    func networkConnectionChecking(completion:@escaping (_ response:String)-> Void){
        if !Connectivity.isConnectedToInternet() {
            UIApplication.shared.endIgnoringInteractionEvents()
            Connectivity.showAlert(title: "SRIS", msg: "No Internet Connection")
            completion("0")
            return
        }else{
            completion("1")
        }
    }
    
    func generateBoundaryString() -> String
    {
        return "\(UUID().uuidString)"
    }
    
    
    func uploadDoc(parametersDict:[String:Any],imageDataDic:[String:Data],uploadUrl:String,completion:@escaping (_ isSuccess:Bool,_ responseDict:NSDictionary) -> Void)
    {
        //NOTE: For upload attachment user below url
        //CXAppConfig.sharedInstance.getBaseUrl() + "Services/uploadMultipleFiles?"
        //for single image with json user posted job url
        //CXAppConfig.sharedInstance.getBaseUrl() + CXAppConfig.sharedInstance.getPlaceOrderUrl()
        CXLog.print(uploadUrl)
        CXLog.print(parametersDict)
        CXLog.print(imageDataDic)
        //Attachments
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (theKey, theValue) in imageDataDic {
                multipartFormData.append(theValue, withName: "srcFile",fileName: theKey, mimeType: "image/jpg")
                CXLog.print(theKey)
            }
            //  multipartFormData.append(imgData, withName: "srcFile",fileName: "addressProof.jpg", mimeType: "image/jpg")
            for (theKey, theValue) in parametersDict {
                multipartFormData.append((theValue as! String).data(using: String.Encoding.utf8)!, withName: theKey )
            }
        },
                         to:uploadUrl,
                         method:.post)
        { (result) in
            switch result {
            case .success(let upload,_, _):
                
                upload.uploadProgress(closure: { (progress) in
                    
                    CXLog.print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
                
                upload.responseJSON { response in
                    CXLog.print(response)
                    completion(true,response.result.value as! NSDictionary)
                }
                
            case .failure(let encodingError):
                
                completion(false,NSDictionary())
            }
        }
    }
    
    func sendTheNotificaton(notificationMesDic:NSDictionary){
        let headers: HTTPHeaders = [
            "Authorization": "key=AIzaSyDy-O-WtgeYLadIGqEFDOcEmq-0fJeEKWo",
            "Accept": "application/json"
        ]
        
        CXLog.print(notificationMesDic)
        Alamofire.request("https://fcm.googleapis.com/fcm/send", method: .post, parameters: notificationMesDic as? Parameters, encoding: JSONEncoding.default, headers: headers).responseString { response in
            print(response.result)
            switch response.result {
            case .success: break
            default :
                break
            }
        }
    }


}


class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func showAlert(title:String,msg:String){
        DispatchQueue.main.async{
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        delegate.window?.rootViewController?.hideLoader()
        delegate.window?.rootViewController?.showAlert(withTitle: title, andMessage: msg)
    }
  }
}




/********************** Usage of Network manager class ****************/

/*let parm = ["type":"Stores" ,"mallId":"23983"]

NetworkingManager.instance.getDataFromServer(path: "http://storeongo.com:8081/Services/getMasters?", parameters: parm as [String : AnyObject], success: { (result) in
    print(result)
    do {
        let responseData = try JSONDecoder().decode(Response.self, from: result)
    } catch let error {
        print(error)
    }
    //"status": 1,
}) { (error) in
    
}
 
 
 struct Response: Decodable{
 let status: Int?
 }
 */
