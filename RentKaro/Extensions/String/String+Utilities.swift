//
//  String+Utilities.swift
//  iOSOngoTemplate
//
//  Created by Mahesh on 23/10/18.
//  Copyright © 2018 ongoframework. All rights reserved.
//

import UIKit

extension String {
    @discardableResult func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: self)
    }
    
    @discardableResult func isValidPhoneNumber() -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return predicate.evaluate(with: self)
    }
    
    @discardableResult func isValidPassword() -> Bool {
        let PHONE_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(\\w{5,})$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return predicate.evaluate(with: self)
    }
    
    @discardableResult func isValidUsername() -> Bool {
        let PHONE_REGEX = "^[a-zA-Z0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return predicate.evaluate(with: self)
    }
    
    static func commonDateFormateString(dateStringIs:String) -> Date {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let date = formatter.date(from: dateStringIs)
        return date!
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            let attStr =  try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
//            attStr.append(NSMutableAttributedString(string: self,
//                                                        attributes: [NSAttributedString.Key.font:ProximaNova-Regular UIFont(name: "ProximaNova-Semibold", size: 16.0)]))
            attStr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "ProximaNova-Regular", size: 14.0), range:  NSMakeRange(0, attStr.length))
            return attStr
        } catch {
            return NSAttributedString()
        }
    }
    
    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        //let list = [json]
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
            options = JSONSerialization.WritingOptions.prettyPrinted
        }
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: options)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            CXLog.print(error)
        }
        return ""
    }
   
}
