//
//  UIViewControllerExtension.swift
//  Vivid
//
//  Created by apple on 10/03/18.
//  Copyright © 2018 ongo. All rights reserved.
//

import UIKit
import MBProgressHUD

 var progress : MBProgressHUD!

 let searchButton = UIButton(type: .custom)
// let cartButton = SSBadgeButton()
// let notificationButton = SSBadgeButton()

func cornersForView(view:UIView, cornerRadius:CGFloat, borderWidth:CGFloat, colour:UIColor) {
    view.layer.cornerRadius = cornerRadius
    view.layer.borderWidth = borderWidth
    view.layer.borderColor = colour.cgColor
}
func cornersForButton(Button:UIButton, cornerRadius:CGFloat, borderWidth:CGFloat, colour:UIColor) {
    Button.layer.cornerRadius = cornerRadius
    Button.layer.borderWidth = borderWidth
    Button.layer.borderColor = colour.cgColor
}
func cornersForImage(image:UIImageView, cornerRadius:CGFloat, borderWidth:CGFloat, colour:UIColor) {
    image.layer.cornerRadius = cornerRadius
    image.layer.borderWidth = borderWidth
    image.layer.borderColor = colour.cgColor
    image.layer.masksToBounds = true
}


extension UIViewController {
    
    
    func backButtonMethod() {
//        self.navigationController?.isNavigationBarHidden = false
//        let backButton = UIButton(type: .custom)
//        backButton.setImage(HGImages.arrow_White_Icon, for: .normal)
//        backButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        backButton.addTarget(self, action: #selector(self.backButtonTapped(sender:)), for: .touchUpInside)
//        let backButtonitem = UIBarButtonItem(customView: backButton)
//        navigationItem.setLeftBarButtonItems([backButtonitem], animated: true)
//        let callBtn = UIButton(type: .custom)
//        callBtn.setImage(HGImages.call_White_Icon, for:.normal)
//        callBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        callBtn.addTarget(self, action: #selector(), for: .touchUpInside)
//        let callBtnItem = UIBarButtonItem(customView: callBtn)
//        navigationItem.setRightBarButtonItems([callBtnItem], animated: true)
//        navigationController?.navigationBar.contentMode = .scaleAspectFit
    }

    //MARK:- Toggle Button Tapped
    @objc func backButtonTapped(sender:UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    //Current Date/Time
    
    func dateFormatTime(date : Date,format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }

    
    func createGradientLayer() {
        
        var gl:CAGradientLayer!
        
//        let colorTop = UIColor.loginGradiantcolor1
//        let colorBottom = UIColor.loginGradiantcolor2
        
        gl = CAGradientLayer()
        //gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        
        self.view.layer.addSublayer(gl)
    }
    
    // use this for all over the project
    func showAlert(withTitle title:String, andMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    // use this for all over the project
    func showAlert(withTitle title:String, andMessage message:String,completion:@escaping (_ success:Bool)->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (alert) in
            completion(true)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithConformation(withTitle title:String, andMessage message:String,completion:@escaping (_ success:Bool)->Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Yes", style: .default) { (alert) in
            completion(true)
        }
        let cancelAction = UIAlertAction(title: "No", style: .default) { (alert) in
            completion(false)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showLoader(message:String){
        progress = MBProgressHUD.showAdded(to: view, animated: true)
        progress.mode = MBProgressHUDMode.indeterminate
        progress.label.text = message
        progress.show(animated: true)
    }
    
    func hideLoader(){
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.9
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func presentViewWithFade(_ viewControllerToPresent: UIViewController, view: UIView) {
        let transition = CATransition()
        transition.duration = 0.9
        transition.type = CATransitionType.fade
        view.layer.add(transition, forKey: kCATransition)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.9
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
    
    func presentDetailLogin(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 1
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetailLoginBack() {
        let transition = CATransition()
        transition.duration = 1
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: (self.view.frame.size.width/2) - (self.view.frame.size.width - 80)/2, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 80, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    

    @objc func cartBarButtonTapped()  {
//        guard let AppointmentVC = StoryBoard.appointmentStoryBoard.instantiateViewController(withIdentifier: AppointmentControllers.appointmentViewController) as? AppointmentViewController else {
//            return
//        }
//        let nav = UINavigationController(rootViewController: AppointmentVC)
//        self.present(nav, animated: true, completion: nil)
        //self.navigationController?.pushViewController(AppointmentVC, animated: true)
        
    }
    
    @objc func notificationBarButtonTapped()  {
//        guard let notificationVC = StoryBoard.dashBoardStoryBoard.instantiateViewController(withIdentifier: DashBoardControllers.notificationViewController) as? NotificationViewController else {
//            return
//        }
//        let nav = UINavigationController(rootViewController: notificationVC)
//        self.present(nav, animated: true, completion: nil)
        //self.navigationController?.pushViewController(AppointmentVC, animated: true)
        
    }
    
    @objc func searchBarButtonTapped()  {
//        guard let searchProductVC = StoryBoard.dashBoardStoryBoard.instantiateViewController(withIdentifier: DashBoardControllers.searchProductViewController) as? SearchProductViewController else {
//            return
//        }
//        let nav = UINavigationController(rootViewController: searchProductVC)
//        self.present(nav, animated: true, completion: nil)
        //self.navigationController?.pushViewController(searchProductVC, animated: true)
        
    }
    
    
    //MARK:- Add ToggleButton Method
    func addToggleButtonMethod()  {
        
        let btn1 = UIButton(type: .custom)
//        btn1.setImage(HGImages.menu_White_Icon!, for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.menuButtonTapped(sender:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
    }
    
    //MARK:- Toggle Button Tapped
   @objc func menuButtonTapped(sender:UIBarButtonItem)  {
//        showLeftViewAnimated(sender)
    }
    
//    func badgeCountMethod(bageCount:Int) {
//        let count = bageCount
//        badgeCountVal = count
//        UserDefaults.standard.set(badgeCountVal, forKey: "badgeCount")
//        UserDefaults.standard.synchronize()
//    }
    
}
    
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func placeHolderColor(textField:UITextField, placeHolderString:String, _ color:UIColor) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString,
                                                             attributes: [NSAttributedString.Key.foregroundColor: color])
    }
}

extension UIView {
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float,corner:CGFloat) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.cornerRadius = corner
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}

struct Device {
    // iDevice detection code
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA = UIScreen.main.scale >= 2.0
    static let SCREEN_WIDTH = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH < 568
    static let IS_IPHONE_5 = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6 = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    static let IS_IPHONE_XMax = IS_IPHONE && SCREEN_MAX_LENGTH > 812
}

