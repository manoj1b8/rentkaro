//
//  Date+Utilities.swift
//  iOSOngoTemplate
//
//  Created by Mahesh on 23/10/18.
//  Copyright © 2018 ongoframework. All rights reserved.
//
import Foundation

enum DateFormat: String {
    case defaultStyle = "yyyy-MM-dd'T'HH:mm:ssZ"
    case backendStyle = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case datePickerStyle = "dd-MMM-yyyy"

}

final class CommonDateFormatter: NSObject {
    static let shared = CommonDateFormatter()
    private var dateFormatter: DateFormatter?
    
    private override init() {
        self.dateFormatter = DateFormatter()
        super.init()
    }
}
