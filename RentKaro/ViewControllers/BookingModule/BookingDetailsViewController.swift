//
//  BookingDetailsViewController.swift
//  RentKaro
//
//  Created by apple on 13/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class BookingDetailsViewController: UIViewController {

    @IBOutlet weak var addOnCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addOnCollectionView.register(UINib.init(nibName: "AddOnCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddOnCollectionViewCell")

    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BookingDetailsViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddOnCollectionViewCell", for: indexPath) as? AddOnCollectionViewCell
        else{
            return UICollectionViewCell()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
