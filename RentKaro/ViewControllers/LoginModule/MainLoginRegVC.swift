//
//  MainLoginRegVC.swift
//  ESahai
//
//  Created by apple on 25/05/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit

class MainLoginRegVC: UIViewController {
    var pageMenu : CAPSPageMenu?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTabViews()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpTabViews(){
        var controllerArray : [UIViewController] = []
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else{
            return
        }
        let nav : UINavigationController = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        controller.title = "Log In"
        controller.viewcontroller = self
        controllerArray.append(nav)
        guard let controller2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController else{
            return
        }
        let nav2 : UINavigationController = UINavigationController(rootViewController: controller2)
        nav.navigationBar.isHidden = true
        controller2.title = "Register"
        controllerArray.append(nav2)
        let parameters: [CAPSPageMenuOption] = [
            .selectionIndicatorColor(UIColor.init(named: "appTheme") ?? UIColor.purple),
            .selectedMenuItemLabelColor(UIColor.init(named: "appTheme") ?? UIColor.purple),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .menuItemWidth(self.view.frame.size.width/2 - 18),
            .scrollMenuBackgroundColor(UIColor.white),
            .menuItemFont(UIFont(name: "Karla-Regular",
                                 size: 16.0)!)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y:70, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        self.view.addSubview((self.pageMenu?.view)!)
    }
}
