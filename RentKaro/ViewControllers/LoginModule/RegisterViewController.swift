//
//  RegisterViewController.swift
//  ESahai
//
//  Created by apple on 25/05/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import ActionSheetPicker_3_0

class RegisterViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var bgViewImage: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var firstNameLbl: UITextField!
    @IBOutlet weak var lastNameLbl: UITextField!
    @IBOutlet weak var mobileNumLbl: UITextField!
    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var passwordLbl: UITextField!
    @IBOutlet weak var confirmPwLbl: UITextField!
    @IBOutlet weak var howDoYouHearLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.mobileNumLbl.delegate = self
        //setUpNavigationBar()
        // Do any additional setup after loading the view.
        viewShadow()
    }
    
    func setUpNavigationBar()
    {
        self.title = AFWrapper.applicationName
        //        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.init(named: "appTheme") ?? UIColor.purple
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
    }
    
    func viewShadow(){
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOpacity = 0.5
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.shadowRadius = 4
        bgView.layer.cornerRadius = 7
    }
    
    @IBAction func registerBtnAct(_ sender: Any) {
        
        if firstNameLbl.text?.count == 0 || firstNameLbl.text?.isEmpty ?? true{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please Enter Your First Name")
        }else if firstNameLbl.text?.count ?? 0 < 3 {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Atleast 3 Characters For First Name ")
        }else if lastNameLbl.text?.count == 0 || lastNameLbl.text?.isEmpty ?? true{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Your Last Name")
        }else if mobileNumLbl.text?.count == 0 || mobileNumLbl.text?.isEmpty ?? true{
             self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Your Mobile Number")
        }else if !(mobileNumLbl.text?.isValidPhoneNumber() ?? false) {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Valid Mobile Number")
        }else if emailLbl.text?.count == 0 || emailLbl.text?.isEmpty ?? true {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Your Email")
        }else if !(emailLbl.text?.isValidEmail() ?? false){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Your Valid Email")
        }else if passwordLbl.text?.count == 0 || passwordLbl.text?.isEmpty ?? true{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Password")
        }else if confirmPwLbl.text?.count == 0 || confirmPwLbl.text?.isEmpty ?? true{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Please Enter Confirm Password")
        }else if passwordLbl.text != confirmPwLbl.text {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Password & Confirm Password Mismatched")
        }else
        {
            GetOTPRegistrationMethod(mobile: self.mobileNumLbl.text ?? "")
        }
    }
    
    func GetOTPRegistrationMethod(mobile:String){
        let mallID = CXAppConfig.sharedInstance.getMallId()
        self.showLoader(message: "Fetching details ..")
        let params = ["orgId":mallID,"mobileNo":mobile] as [String : AnyObject]
        let otpUrlStr = CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getRegOTP()
        CXDataService.sharedInstance.getDataFromServer(path:otpUrlStr, parameters: params, success: { (response) in
    
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(OTPModel.self, from:
                    response) //Decode JSON Response Data
                CXLog.print(model)
                self.hideLoader()
                if model.status == "1"{
                    self.otpView(otp: model.OTP ?? "")
                }else{
                    self.showAlert(withTitle: AFWrapper.applicationName, andMessage: model.msg ?? "")
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }) { (failureResponse) in
            self.hideLoader()
            CXLog.print(failureResponse)
        }
        
    }
    
    func otpView(otp:String){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let otpVC = storyBoard.instantiateViewController(withIdentifier: "OtpViewController") as? OtpViewController else{
            return
        }
        otpVC.modalPresentationStyle = .overCurrentContext
        otpVC.modalTransitionStyle = .crossDissolve
        let popover = otpVC.popoverPresentationController
        popover?.permittedArrowDirections = .any
        otpVC.emailText = self.emailLbl.text ?? ""
        otpVC.firstNameText = self.firstNameLbl.text ?? ""
        otpVC.lastNameText = self.lastNameLbl.text ?? ""
        otpVC.passwordText = "123456"
        otpVC.mobileNumberText = self.mobileNumLbl.text ?? ""
        otpVC.comingFrom = "RegisterView"
        otpVC.serverOTP = otp
        let navCOntrol = UINavigationController(rootViewController: otpVC)
        self.present(navCOntrol, animated: true, completion: nil)
    }
    
    @IBAction func dropDownBtnAction(_ sender: Any) {
        CXDataService.sharedInstance.showPickerMethod(title: "", rows: [["Google","Facebook","Other"]], initialSelection: [0], completionPicking: { (arr, index) in
            let selectedOption = arr[0]
            //let selectedIndex:Int = index[0] as? Int ?? 0
            self.howDoYouHearLbl.text = selectedOption
        }, controller: self)
    }
}

extension RegisterViewController{
    
    //MARK: Should change characters in range
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.mobileNumLbl{
            if range.location > 9 {
                textField.resignFirstResponder()
                return false
            }
        } else if textField == self.passwordLbl{
            if range.location > 7 {
                textField.resignFirstResponder()
                return false
            }
        } else if textField == self.confirmPwLbl {
            if range.location > 7 {
                textField.resignFirstResponder()
                return false
            }
        }
        
        return true
    }
    
}

