//
//  ConfirmPasswordViewController.swift
//  PattabhiAgro
//
//  Created by OngoApple on 12/01/18.
//  Copyright © 2018 ongoStore. All rights reserved.
//

import UIKit
import RealmSwift

class ConfirmPasswordViewController: UIViewController {
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var enterConfirmPasswordTextField: CXTextField!
    @IBOutlet weak var enterNewPasswordTextField: CXTextField!
    
    var mobileNumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.confirmPasswordView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        
        // Do any additional setup after loading the view.
    }
    
    
    func forgotPasswordApiCall(){
        
        //        let realm = try! Realm()
        //        let profile = realm.objects(MRDoctorPRofile.self).first
        //        
        //        CXDataService.sharedInstance.showLoader(view: self.view, message: "Loading...")
        //        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getForgotPassordUrl(),parameters: ["orgId":CXAppConfig.sharedInstance.getAppMallID() as NSCopying,"email":profile?.email as! NSCopying,"password":self.enterConfirmPasswordTextField.text! as NSCopying]) { (responceDict) in
        //            CXDataService.sharedInstance.hideLoader()
        //            let message = responceDict.value(forKey: "result") as? String
        //            let status: Int = Int(responceDict.value(forKey: "status") as! String)!
        //            if status == 1{
        //                self.showAlert(message!, status: 1)
        //            }else{
        //                self.showAlert(message!, status: 0)
        //            }
        //        }
    }
    
    func showAlert(_ message:String, status:Int) {
        let alert = UIAlertController(title: "Alert!!!", message:message , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if status == 1 {
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.navigateToLogin()
            }
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //http://54.151.192.16:8081/MobileAPIs/forgotpwd?orgId=5&email=8500415074@ongo.com&password=123123
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        
        
        if validationTextFields(){
            
            if self.enterNewPasswordTextField.text != self.enterConfirmPasswordTextField.text{
                CXDataService.sharedInstance.showAlert(message: "your Enter confirm Password Does Not match with new Password", viewController: self)
            }
            let email = UserDefaults.standard.value(forKey: "userMail") as? String
            let mobileNumber = UserDefaults.standard.value(forKey: "mobileNumber") as? String
            self.showLoader( message: "Loading...")
            CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getForgotPassordUrl(),parameters: ["orgId":CXAppConfig.sharedInstance.getMallId() as NSCopying,"email":"\(String(describing: email!))" as NSCopying,"password":self.enterConfirmPasswordTextField.text! as NSCopying]) { (responceDict) in
                    self.hideLoader()
//                let message = responceDict.values(forKey: "result") as? String
//                let status: Int = Int(responceDict.values(forKey: "status") as! String)!
//                if status == 1{
//                    self.showAlert(message!, status: 1)
//                }else{
//                    self.showAlert(message!, status: 0)
//                }
            }
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func validationTextFields() -> Bool{
        
        
        if self.enterNewPasswordTextField.text?.count == 0{
            
        }else if self.enterConfirmPasswordTextField.text?.count == 0{
            
        }else{
            return true
        }
        
        return false
    }
}
