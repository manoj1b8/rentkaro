//
//  LoginViewController.swift
//  ESahai
//
//  Created by apple on 25/05/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class LoginViewController: UIViewController,UITextFieldDelegate {
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var bgView: CXView!
    @IBOutlet weak var contactNumTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    var dict : [String : AnyObject]!
    var faceBookDict : [String : AnyObject]!
    var socialId = String()
    var socialName = String()
    var firstName = String()
    var lastName = String()
    var socialMail = String()
    var socialImgUrlStr = String()
    var userFbdetailsDict = NSMutableDictionary()
    var viewcontroller = MainLoginRegVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewShadow()
        setUpNavigationBar()
        self.contactNumTf.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func viewShadow(){
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOpacity = 0.5
        bgView.layer.shadowRadius = 4
        bgView.layer.cornerRadius = 7
    }
    //MARK:- setup navigarion bar
    func setUpNavigationBar()
    {
        self.title = AFWrapper.applicationName
        self.navigationController?.navigationBar.barTintColor = UIColor.init(named: "appTheme") ?? UIColor.purple
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
    }
    //MARK:- Login Button Action
    @IBAction func loginBtnAct(_ sender: Any) {
        
        if self.contactNumTf.text?.count == 0 || self.contactNumTf.text?.isEmpty ?? true {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:  "Enter Your Mobile Number")
        }
        else if !(self.contactNumTf.text?.isValidPhoneNumber() ?? false) {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Enter Valid Mobile Number")
        }
        else if self.passwordTf.text?.isEmpty ?? true {
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Enter Password")
        }else{
            LoginAPIIntegration(mobile: contactNumTf.text ?? "", password: passwordTf.text ?? "")
            
        }
        
    }
    //MARK:- Login API :http://149.129.173.73:8081/MobileAPIs/loginConsumerForOrg?orgId=113&mobile=9704239991&dt=DEVICES&password=123456
    
    //MARK:- Check the User Data in Server
    func LoginAPIIntegration(mobile:String ,password:String){
        let mallID = CXAppConfig.sharedInstance.getMallId()
        self.showLoader(message: "Fetching details ..")
        var params =  ["":""]
        params["orgId"] = CXAppConfig.sharedInstance.getMallId()
        params["mobileNo"] = mobile
        params["password"] = password
        params["dt"] = "DEVICES"
        let loginUrlStr = CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getLoginOTP()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(loginUrlStr, parameters: params as [String : AnyObject]) { (response) in
            CXLog.print(response)
            self.hideLoader()
            let message =  response["msg"] as? String ?? ""
            if response["status"] as? String ?? "" == "1" || response["status"] as? Int ?? 0 == 1 {
                UserDataManager.sharedInstance.saveTheUserDetails(userDataDic: response, completion: { (success) in
                    if success {
                        self.showAlert(withTitle: AFWrapper.applicationName, andMessage: message, completion: { (succeed) in
                            AFWrapper.appDelegate?.setUpSidePanel()
                        })
                    }
                })
            }else if response["status"] as? String ?? "" == "-1" {
                self.showAlert(withTitle: AFWrapper.applicationName, andMessage: message)
            }else{
                self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Something went wrong!")
            }
        }
    }
    
    //http://149.129.173.73:8081/Services/getMasters?type=MacIdInfo&mallId=2
    //http://149.129.173.73:8081/MobileAPIs/Services/getMasters?type=Resource+Allocation&consumerId=1204
    //MARK:- Get the user info
    /* func getTheUserDetailsFromServer(mobile:String) {
     self.showLoader(message: "Fetching details ..")
     let mallID = CXAppConfig.sharedInstance.getMallId()
     let userDataDic = ["mallId":mallID,"keyWord":mobile,"type":"macIdInfo","propKey":"mobileNo"] as [String : Any]
     let loginUrlStr = CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getMasterUrl()
     CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(loginUrlStr, parameters: (userDataDic as [String : AnyObject]?)!) { (responseDic) in
     CXLog.print(responseDic)
     self.hideLoader()
     if  CXAppConfig.resultString(input: responseDic["status"]!) == "1" && CXAppConfig.resultString(input: responseDic["totalNumRecords"]!) != "0" {
     let jobs = responseDic["jobs"] as! [AnyObject]
     let dict = jobs[0] as? [String:AnyObject]
     CXLog.print(dict!)
     CXDataSaveManager.sharedInstance.saveTheUserDetails(userDataDic: dict!, completion: { (response) in
     if response{
     delegate.navToHome()
     CXLog.print("Data saved in realm Database")
     
     }
     })
     }
     }
     } */
    
    @IBAction func forgotPwBtnAct(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let referralVC = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController else{
            return
        }
        self.viewcontroller.present(referralVC, animated: true, completion: nil)
    }
    
    func clearData() -> Void {
        contactNumTf.text = ""
    }
    
    func OTPView(otp:String){
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let referralVC = storyBoard.instantiateViewController(withIdentifier: "OtpViewController") as? OtpViewController else{
            return
        }
        referralVC.modalPresentationStyle = .overCurrentContext
        referralVC.modalTransitionStyle = .crossDissolve
        let popover = referralVC.popoverPresentationController
        popover?.permittedArrowDirections = .any
        referralVC.mobileNumberText = self.contactNumTf.text ?? ""
        referralVC.comingFrom = "LoginVeiw"
        referralVC.serverOTP = otp
        let navCOntrol = UINavigationController(rootViewController: referralVC)
        self.present(navCOntrol, animated: true, completion: nil)
    }
    
    //MARK: Should change characters in range
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.contactNumTf{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 10
        }
        return true
    }
    
}


