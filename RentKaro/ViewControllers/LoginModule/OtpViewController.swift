//
//  OtpViewController.swift
//  Lifeline
//
//  Created by Rama Kuppa on 7/10/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift

class OtpViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var otpEnterTf: UITextField!
    var emailText = String()
    var firstNameText = String()
    var lastNameText = String()
    var passwordText = String()
    var stateText = String()
    var cityText = String()
    var serverOTP = String()
    var mobileNumberText = String()
    var comingFrom = String()
    var countdownTimer = Timer()
    var totalTime = 25
    
    @IBOutlet weak var resendOtpTimerLabel: UILabel!
    @IBOutlet weak var otpBackView: UIView!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var resendBtn: CXButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contactNumberLabel.text = self.mobileNumberText
        self.navigationController?.navigationBar.isHidden = true
        self.resendBtn.isHidden = true
        self.otpEnterTf.delegate = self
        self.viewShadow()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.countdownTimer.invalidate()
    }
    //MARK:- Add shadow to view
    func viewShadow(){
        otpBackView.layer.shadowColor = UIColor.black.cgColor
        otpBackView.layer.shadowOpacity = 0.5
        otpBackView.layer.shadowOffset = CGSize.zero
        otpBackView.layer.shadowRadius = 4
        otpBackView.layer.cornerRadius = 7
    }
    
    //MARK:- Start Timer Method
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    //MARK:- Update Time Method
    @objc func updateTime() {
        let time = timeFormatted(totalTime)
        self.resendOtpTimerLabel.text = "Resend 4 Digit OTP? \(time)s"
        if totalTime != 0 {
            self.resendBtn.isHidden = true
            totalTime -= 1
        } else {
            self.resendBtn.isHidden = false
            self.resendOtpTimerLabel.text = "Resend 4 Digit OTP?"
            endTimer()
        }
    }
    
    //MARK:- End Timer Method
    func endTimer() {
        countdownTimer.invalidate()
    }
    
    //MARK:- Time Format Method
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        return String(format: "%02d", seconds)
    }
    
    //MARK: Should change characters in range
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.otpEnterTf{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 6
        }
        return true
    }
    
    //MARK:- Login API Integration
    func LoginAPIIntegration(mobile:String,password:String){
        self.showLoader(message: "Signing in..")
        var params =  ["":""]
        params["orgId"] = CXAppConfig.sharedInstance.getMallId()
        params["mobileNo"] = mobile
        params["password"] = password
        params["dt"] = "DEVICES"
        let signinUrl = CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getSignInUrl()
        CXDataService.sharedInstance.getDataFromServer(path:signinUrl, parameters: params as [String : AnyObject], success: { (response) in
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(OTPModel.self, from:
                    response) //Decode JSON Response Data
                CXLog.print(model)
                if model.status == "1"{
                    
                }else{
                    self.showAlert(withTitle: AFWrapper.applicationName, andMessage: model.msg ?? "")
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
            
            self.hideLoader()
        }) { (failureResponse) in
            self.hideLoader()
            CXLog.print(failureResponse)
        }
    }
    
    //MARK:- Test Registration Link :  http://149.129.172.43:8081/MobileAPIs/regAndloyaltyAPI?orgId=2&userEmailId=1234567892@ongo.com&mobileNo=1234567892&firstName=test1&lastName=testing&password=123123&jsonString={%22UserType%22:%22Customer%22}
    
    //MARK:- Registration API Integration
    func RegistrationAPIIntegration(){
        self.showLoader(message: "Processing your registration..")
        var params =  ["":""]
        params["orgId"] = CXAppConfig.sharedInstance.getMallId()
        params["firstName"] = firstNameText
        params["lastName"] = lastNameText
        params["userEmailId"] = emailText
        params["mobileNo"] = mobileNumberText
        params["password"] = passwordText
        params["UserType"] = "Customer"
        let registerUrl = CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getSignUpInUrl()
        CXDataService.sharedInstance.synchDataToServerAndServerToMoblile(registerUrl, parameters: params as [String : AnyObject]) { (response) in
            CXLog.print(response)
            self.hideLoader()
            let message =  response["msg"] as? String ?? ""
            if response["status"] as? String ?? "" == "1" || response["status"] as? Int ?? 0 == 1 {
                UserDataManager.sharedInstance.saveTheUserDetails(userDataDic: response, completion: { (success) in
                    if success {
                        self.showAlert(withTitle: AFWrapper.applicationName, andMessage: message, completion: { (succeed) in
                            AFWrapper.appDelegate?.setUpSidePanel()
                        })
                    }
                })
            }else if response["status"] as? String ?? "" == "-1" {
                self.showAlert(withTitle: AFWrapper.applicationName, andMessage: message)
            }else{
                self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Something went wrong!")
            }
        }
    }
    
    func clearData() -> Void {
        self.mobileNumberText = ""
    }
    
    @IBAction func loginBtnAction(_ sender: CXButton) {
        if validationOTP(){
            if self.comingFrom == "ForgotView"{
                let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let confirmPasswordVC = storyBoard.instantiateViewController(withIdentifier: "ConfirmPasswordViewController") as! ConfirmPasswordViewController
                self.present(confirmPasswordVC, animated: true, completion: nil)
            }else if self.comingFrom == "LoginVeiw"{
                self.LoginAPIIntegration(mobile: self.mobileNumberText, password: self.passwordText)
            }else if self.comingFrom == "RegisterView"{
                self.RegistrationAPIIntegration()
            }
        }
        
    }
    
    func validationOTP() -> Bool{
        if self.otpEnterTf.text?.count == 0{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter OTP")
        }else if self.otpEnterTf.text != serverOTP{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"Invalid OTP")
        }else{
            return true
        }
        return false
    }
    
    //http://localhost:8081/MobileAPIs/loginWithOTP?mobileNo=8179716466&orgId=3
    @IBAction func resendBtnAct(_ sender: Any) {
        self.totalTime = 25
        self.startTimer()
        
    }
    
    @IBAction func backBtnACtion(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Merge Two Dictionaries Method
extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
