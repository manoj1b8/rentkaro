//
//  ProfileViewController.swift
//  ESahai
//
//  Created by apple on 24/05/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import MobileCoreServices
import AVFoundation
import Photos

class ProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //MARK:- OUTLETS
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameLbl: UITextField!
    @IBOutlet weak var lastNameLbl: UITextField!
    @IBOutlet weak var mobileNumLbl: UITextField!
    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var bgView: UIView!
     @IBOutlet weak var saveBtn: CXButton!
    @IBOutlet weak var imageEditBtn: CXButton!
    
    //MARK:- Variables
    var imagePath = ""
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let imagePickerController = UIImagePickerController()
    var jsonParameters = [String:String]()
     var imageDict = [String:Data]()
   
    //MARK:- View Controller Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveBtn.setTitle("EDIT", for: .normal
        )
        self.firstNameLbl.isUserInteractionEnabled = false
        self.lastNameLbl.isUserInteractionEnabled = false
        self.imageEditBtn.isUserInteractionEnabled = false
        self.imageEditBtn.isHidden = true
        setUpNavigationBar()
        profileInfo()
        self.imagePickerController.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.revealViewController().delegate = self
        self.revealViewController().panGestureRecognizer().isEnabled = true
        self.revealViewController().tapGestureRecognizer().isEnabled = true
    }
    
    //MARK:- Navigationbar Setup
    func setUpNavigationBar(){
        self.title = "Profile"
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 52, height: 52)
        btn1.addTarget(self, action: #selector(menuBtnAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
        self.navigationController?.navigationBar.barTintColor = UIColor.navBarColor
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    @objc func menuBtnAction(){
        self.revealViewController().revealToggle(UIButton())
    }
    
    //MARK:- Check For Profile
    func profileInfo(){
        let relamInstance = try! Realm()
        let userData = relamInstance.objects(UserAccountDB.self).first
        self.firstNameLbl.text = userData?.firstName
        self.lastNameLbl.text = userData?.lastName
        self.mobileNumLbl.text = userData?.mobileNo
        self.emailLbl.text = userData?.Email
        self.profileImageView.sd_setImage(with:  URL(string: userData?.userImagePath ?? ""), placeholderImage: UIImage(named: "defaultIcon"))
    }
    
    //MARK:- Image Edit Button Action
    @IBAction func imageEditBtnAction(_ sender: CXButton) {
        OperationQueue.main.addOperation {
            let alertController = UIAlertController(title: AFWrapper.applicationName, message: nil, preferredStyle: .actionSheet)
            let takePhotoAction = UIAlertAction(title: "TakePhoto", style: .default) { (UIAlertAction) in
                self.cameraPresent()
            }
            let libraryAction = UIAlertAction(title:"Choose From Library", style: .default) { (UIAlertAction) in
                self.galleryPresent()
            }
            let cancelAction = UIAlertAction(title:"Cancel", style: .cancel)
            { (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(takePhotoAction)
            alertController.addAction(libraryAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- Toggle Button For Editing and Saving Profile
    @IBAction func saveBtnAction(_ sender: UIButton) {
        if sender.titleLabel?.text == "EDIT"{
            self.saveBtn.setTitle("SAVE", for: .normal
            )
            self.firstNameLbl.isUserInteractionEnabled = true
            self.lastNameLbl.isUserInteractionEnabled = true
            self.imageEditBtn.isUserInteractionEnabled = true
            self.imageEditBtn.isHidden = false
        }else{
            self.saveBtn.setTitle("EDIT", for: .normal
            )
            self.firstNameLbl.isUserInteractionEnabled = false
            self.lastNameLbl.isUserInteractionEnabled = false
            self.imageEditBtn.isUserInteractionEnabled = false
            self.imageEditBtn.isHidden = true
            self.profileUpdate()
        }
    }
    
    //MARK: - Persent Camera
    func cameraPresent()  {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
            openCamera()
        case .denied:
            self.showImagesAlertMessages(title: "allowCameraSettings")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted == true {
                    self.openCamera()
                }else {
                    self.showImagesAlertMessages(title: "allowCameraSettings")
                }
            })
        default:
            print("Not permission")
        }
    }
    
    //MARK: - Open Camera
    func openCamera()  {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePickerController.mediaTypes = [kUTTypeImage as String]
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }else {
            self.noCamera()
        }
    }
    
    //MARK: - Gallery Present
    func galleryPresent()  {
        let photos = PHPhotoLibrary.authorizationStatus()
        switch photos {
        case .authorized:
            openGallery()
        case .denied:
            showImagesAlertMessages(title: "allowGallerySettings")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (granted) in
                switch granted {
                case .authorized:
                    self.openGallery()
                case.denied,.restricted:
                    self.showImagesAlertMessages(title: "allowGallerySettings")
                default:
                    print("Not Permission")
                }
            })
        default:
            print("Not Permision")
        }
    }
    
    //MARK: - Show Alert For Camera Permission
    func showImagesAlertMessages(title:String)  {
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style:.default) { (UIAlertAction) in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Open Gallery
    func openGallery()  {
        self.imagePickerController.mediaTypes = [kUTTypeImage as String]
        self.imagePickerController.sourceType = .photoLibrary
        self.imagePickerController.allowsEditing = true
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    //MARK:- No Camera Mehod
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style:.default, handler: nil)
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
  
    //MARK:- Image Picker Delegate Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let image = pickedImage as UIImage
            let orientationFixedImage = image.fixOrientation()
            let data = orientationFixedImage.jpeg(.low)
            self.imageDict["UserImage.jpg"] = data
            self.jsonParameters["Image"] = "UserImage.jpg"
            self.profileImageView.image = image
            self.getImageurlString(data:data ?? Data())
        }
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
       // self.isEditButtonTapped = true
    }
    
    //MARK:- Get Image Url
    func getImageurlString(data:Data){
        self.showLoader( message: "Loading...")
        CXDataService.sharedInstance.imageUpload(data as Data) { (Response) in
            print("\(Response)")
            let status: Int = Int(Response.value(forKey: "status") as! String)!
            if status == 1{
                DispatchQueue.main.async(execute: {
                    let imgStr = Response.value(forKey: "filePath") as! String
                    UserDefaults.standard.setValue(imgStr, forKey: "USER_IMAGE_PATH")
                    self.hideLoader()
                    self.jsonParameters["Image"] = imgStr
                    //self.profileUpdate()
                    self.imagePath = imgStr
                })
            }else {
                UserDefaults.standard.set("", forKey: "IMAGE_PATH")
            }
        }
    }

    //MARK:- Profile API Integration
    func profileUpdate(){
        let updateProfileUrl = CXAppConfig.sharedInstance.getBaseUrl()+CXAppConfig.sharedInstance.getUpdateMultipleProperties()
        var jsonParams = [String:String]()
        jsonParams["firstName"] = self.firstNameLbl.text ?? ""
        jsonParams["lastName"] = self.lastNameLbl.text ?? ""
        self.jsonParameters = self.jsonParameters as Dictionary
        self.jsonParameters.merge(dict: self.jsonParameters)
        let userdata = UserDataManager.sharedInstance.getTheUserProfileFromDB()
        let params = ["ownerId":CXAppConfig.sharedInstance.getMallId() as AnyObject,
                      "jobId": "\(userdata)",
            "jsonString":String.gernateJsonObjec(dataDic: jsonParameters as NSDictionary)] as [String : Any]
       self.showLoader(message: "Loading...")
        UIApplication.shared.beginIgnoringInteractionEvents()
        CXDataService.sharedInstance.uploadDoc(parametersDict: params, imageDataDic: self.imageDict, uploadUrl: updateProfileUrl, completion: { (success, response) in
            self.hideLoader()
            UIApplication.shared.endIgnoringInteractionEvents()
            CXLog.print(response)
            let message = response["message"] as AnyObject
            let status = "\(response["status"] ?? 0 as AnyObject)"
            if status == "1" {
                    DispatchQueue.main.async(execute: {
                        self.hideLoader()
                        self.showAlert(withTitle: AFWrapper.applicationName, andMessage:"\(message)", completion: { (true) in
                
                        })
                        self.updateTheProfileInLocalDB()
                   
                    })
                
            }else{
                self.hideLoader()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        })
    }
    
    //MARK:- Profile Update in Local DB
    func updateTheProfileInLocalDB(){
        let realm = try! Realm()
        let profile =  realm.objects(UserAccountDB.self)
        try! realm.write {
            profile[0].userImagePath = self.imagePath
            profile[0].firstName = self.firstNameLbl.text ?? ""
            profile[0].lastName = self.lastNameLbl.text ?? ""
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("updateImageNotification"), object: nil)
            }
        }
    }
}

//MARK:- SWRevealViewController Delegate Method
extension ProfileViewController : SWRevealViewControllerDelegate{
    
    func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        if revealController.frontViewPosition == FrontViewPositionLeft {
            self.view.isUserInteractionEnabled = true
        }
        else {
            self.view.isUserInteractionEnabled = false
        }
    }
}
