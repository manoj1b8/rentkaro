//
//  ForgotPasswordViewController.swift
//  Lotus
//
//  Created by Rama kuppa on 05/12/17.
//  Copyright © 2017 ongo. All rights reserved.
//

import UIKit
import  RealmSwift
import IQKeyboardManagerSwift

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLETS
    @IBOutlet weak var userMailTextField: CXTextField!
    @IBOutlet weak var forgotPasswordView: UIView!
    
    //MARK:- Variables
    var mibileNumberArray = NSArray()
    var mobileNumberDict = NSDictionary()
    var forgotPasswordOtpString = NSString()
    var otpTextField = UITextField()
    var message : String?
    var otp : String?
    var mobile = String()
    var forgot : OtpViewController?
    
    //MARK:- View Controller Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.forgotPasswordView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        self.userMailTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.forgot?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Should change characters in range
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.userMailTextField{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 10
        }
        return true
    }
    

    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
     
    }
    
    func comparingFieldWithOTP(){
    
    }
    
    
    func isValidEmail(_ email: String) -> Bool {
        CXLog.print("validate email: \(email)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: email) {
            return true
        }
        return false
    }
    
    func validationTextFields() -> Bool{
        
        if self.userMailTextField.text?.count == 0{
            
            CXDataService.sharedInstance.showAlert(message: "Please Enter your Registered MobileNumber", viewController: self)
        }else{
            
            return true
        }
        return false
    }
    
    func showAlert(_ message:String, status:Int) {
        let alert = UIAlertController(title: "Alert!!!", message:message , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if status == 1 {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
