//
//  SearchViewController.swift
//  RentKaro
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchTableView: UITableView!
    
    var searchName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.switchBetweenVC()
    }
    
    func switchBetweenVC(){
        self.titleLabel.text = self.searchName
        switch searchName {
        case SearchType.Model.rawValue:
            self.searchBar.placeholder = "Search by model"
            return
        default:
            self.searchBar.placeholder = "Search by variant"
            return
        }
    }

    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell") as? SearchTableViewCell else{
            return UITableViewCell()
        }
        return cell
    }
    
    
}

enum SearchType:String{
    case Model,Variant
}
