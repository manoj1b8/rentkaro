//
//  MyVehicleViewController.swift
//  RentKaro
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class MyVehicleViewController: UIViewController {

    @IBOutlet weak var myVehicleTableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerNibFile()
        
    }
    
    func registerNibFile(){
        self.myVehicleTableView.register(UINib.init(nibName: "MyVehicleTableViewCell", bundle: nil), forCellReuseIdentifier: "MyVehicleTableViewCell")
        self.myVehicleTableView.tableHeaderView = nil
        self.myVehicleTableView.tableFooterView = self.footerView
    }
    

}

extension MyVehicleViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyVehicleTableViewCell") as?MyVehicleTableViewCell else{
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
}
