//
//  AddVehicleDtailsViewController.swift
//  RentKaro
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AddVehicleDetailsViewController: UIViewController {

    
    @IBOutlet weak var modelTF: UITextField!
    @IBOutlet weak var variantTF: UITextField!
    @IBOutlet weak var makeYearTF: UITextField!
    @IBOutlet weak var colorTF: UITextField!
    @IBOutlet weak var licenseNoTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    
    var manuYearArr = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generatingYears()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func validateFields()->Bool{
        if self.modelTF.text?.isEmpty ?? true{
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select model")
            return false
        }else if(self.variantTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select variant")
            return false
        }else if(self.makeYearTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select make year")
            return false
        }else if(self.colorTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter color")
            return false
        }else if(self.licenseNoTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter license number")
            return false
        }else if(self.addressTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter address")
            return false
        }else{
            return true
        }
    }
    
    func generatingYears(){
        let currentDate = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: currentDate)
        self.manuYearArr = (2000...currentYear).map { $0 }.reversed()
        
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func modelBtnAction(_ sender: Any) {
        guard let searchVC = UIStoryboard.init(name: "FindParking", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else{
            return
        }
        searchVC.searchName = "Model"
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @IBAction func variantBtnAction(_ sender: Any) {
        guard let searchVC = UIStoryboard.init(name: "FindParking", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else{
            return
        }
        searchVC.searchName = "Variant"
        self.navigationController?.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func makeYearBtnAction(_ sender: Any) {
        CXDataService.sharedInstance.showPickerMethod(title: "select year", rows: [self.manuYearArr], initialSelection: [0], completionPicking: { (selectedYear, selectedIndex) in
            self.makeYearTF.text = "\(selectedYear.first ?? "")"
        }, controller: self)
        
    }
    
    
    @IBAction func addVehicleBtnAction(_ sender: Any) {
        if validateFields(){
            
        }
    }
}
