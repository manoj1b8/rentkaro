
//
//  DashBoardViewController.swift
//  ESahai
//
//  Created by Rama kuppa on 4/9/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import CoreLocation

class DashBoardViewController: UIViewController{

    @IBOutlet weak var dashBoardCollectionView: UICollectionView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mainView: UIView!
    
    var itemSelect = String()
    var lat : String?
    var longi : String?
    var markerTitle : String?
    let locationManager = CLLocationManager()
    var nameArray = [String]()
    var imagesArray = [String]()
    var imagesArrayGray = [String]()
    var selectedIndex = -1
    var currentAddress = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
         nameArray = ["FIND PARKING","PARKING AT AIRPORT"]
         imagesArray = ["findParkingIcon","parkingAtAirportSelectedIcon"]
         imagesArrayGray = ["findParkingIcon","parkingAtAirportSelectedIcon"]
         self.collectionViewCode()
         setUpSideMenu()
         self.loadMapView()
        //Raviteja
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.revealViewController().delegate = self
        self.revealViewController().panGestureRecognizer().isEnabled = true
        self.revealViewController().tapGestureRecognizer().isEnabled = true
    }
    
    func collectionViewCode() -> () {
        self.dashBoardCollectionView.dataSource = self
        self.dashBoardCollectionView.delegate = self
  }
    
    func setUpSideMenu(){
        self.title = AFWrapper.applicationName
        let menuItem = UIBarButtonItem(image: UIImage(named: "sideMenu"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        self.navigationItem.leftBarButtonItem = menuItem
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.init(named: "appTheme") ?? UIColor.purple
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "custCare"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 52, height: 52)
        //btn2.addTarget(self, action: #selector(customerCareSupport), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.rightBarButtonItem = item2
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18.0)
        ]
        
    }
    
    //MARK:- Google Maps search interface VC
    func moveForSearchInMap(){
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        self.present(placePickerController, animated: true, completion: nil)
    }
    
    @IBAction func nextBtnTap(_ sender: Any) {
        if self.selectedIndex == 0{
            //self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Coming Soon")
            guard let addVehicleDetailsVC = UIStoryboard.init(name: "FindParking", bundle: nil).instantiateViewController(withIdentifier: "AddVehicleDetailsViewController") as? AddVehicleDetailsViewController else{
                return
            }
            self.navigationController?.pushViewController(addVehicleDetailsVC, animated: true)
        }else{
            guard let flightDetailsVC = storyboard?.instantiateViewController(withIdentifier: "FlightDetailsViewController") as? FlightDetailsViewController else{
                return
            }
            self.navigationController?.pushViewController(flightDetailsVC, animated: true)
        }
    }
    
    @IBAction func moveToMapsBtnAction(_ sender: Any) {
        self.moveForSearchInMap()
    }
    

}
extension DashBoardViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCell", for: indexPath) as? DashBoardCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.backView.layer.borderColor = (UIColor.init(named: "appTheme") ?? UIColor.purple).cgColor
        cell.backView.layer.borderWidth = 0.5
        cell.backView.layer.cornerRadius = 6
        cell.backView.layer.masksToBounds = true
        cell.tittleLbl.setTitle(nameArray[indexPath.row], for: .normal)
        cell.tittleLbl.tag = indexPath.row
        if (self.selectedIndex == -1) || (selectedIndex == indexPath.row) {
            if self.selectedIndex == -1{
               self.selectedIndex = indexPath.row
            }
            cell.tittleLbl.backgroundColor = UIColor.init(named: "appTheme") ?? UIColor.purple
            cell.tittleLbl.setTitleColor(UIColor.white, for: .normal)
            cell.imgView.image = UIImage(named: imagesArray[indexPath.row])
        }else{
            cell.tittleLbl.backgroundColor = UIColor.white
            cell.tittleLbl.setTitleColor(UIColor.init(named: "appTheme") ?? UIColor.purple, for: .normal)
            cell.imgView.image = UIImage(named: imagesArrayGray[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.selectedIndex == indexPath.row{
            
        }else{
            self.selectedIndex = indexPath.row
            self.dashBoardCollectionView.reloadData()
        }
    }
}

//MARK: SWRevealViewController Delegate Method
extension DashBoardViewController : SWRevealViewControllerDelegate{
    
    func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        if revealController.frontViewPosition == FrontViewPositionLeft {
            self.view.isUserInteractionEnabled = true
        }
        else {
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension DashBoardViewController :GMSMapViewDelegate,CLLocationManagerDelegate{
    
    func loadMapView(){
        
    if let title = self.markerTitle, let lat = self.lat, let long = self.longi, !title.isEmpty, !lat.isEmpty, !long.isEmpty {
    
    let lat = Float(lat)
    let long = Float(long)
    
    let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat ?? 0.0), longitude: CLLocationDegrees(long ?? 0.0), zoom: 17.0)
    self.mapView.camera = camera
    let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat ?? 0.0), longitude: CLLocationDegrees(long ?? 0.0))
    let marker = GMSMarker(position: position)
    marker.title = self.markerTitle
    marker.icon = UIImage(named:"annotation")
    marker.map = self.mapView
    self.mapView.isMyLocationEnabled = true
    self.mapView.settings.compassButton = true
    self.mapView.settings.myLocationButton = true
    }else{
        self.initializeTheLocationManager()
 
    }
}
    
    func initializeTheLocationManager() {
        locationManager.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.compassButton = true
        self.mapView.settings.myLocationButton = true
        self.locationManager.startUpdatingLocation()

        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = 100.0; // Will notify the LocationManager every 100 meters
        self.isAuthorizedtoGetUserLocation()
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last!
        let cityCoords = CLLocation(latitude: (location.coordinate.latitude) ?? 0.0, longitude:(location.coordinate.longitude) ?? 0.0)
        let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 17.0)
        
        self.mapView?.animate(to: camera)
        getAddressDetails(coordinates: location.coordinate)
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
    }
    
    func  getAddressDetails(coordinates: CLLocationCoordinate2D) {
        CXLog.print("coordinate \(coordinates)")
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinates) { response, error in
            if let address = response?.firstResult() {
                let addressLine = "\(address.subLocality ?? ""),\(address.locality ?? ""),\(address.administrativeArea ?? ""),\(address.postalCode ?? ""),\(address.country ?? "")"
              
            }
        }
    }

}

//MARK:- GMSAutocomplete Delegate methods
extension DashBoardViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.getAddressDetails(coordinates: place.coordinate)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
