//
//  SideMenuViewController.swift
//  RaviInfotech
//
//  Created by Rama kuppa on 2/14/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class SideMenuViewController: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userID: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var userImageView: CXImageView!
    
    //MARK:- Variables
    var tittleImgs = [Sidemenu]()
    var topTittle = [Sidemenu]()
    
    //MARK:- View controller Life Cycle Methods
    override func viewDidLoad() {
        tableDelegates()
        profileInfo()
        super.viewDidLoad()
        topTittle = [Sidemenu.home,Sidemenu.profie,Sidemenu.bookings,Sidemenu.settings,Sidemenu.ContactUs]
        tittleImgs = [Sidemenu.home,Sidemenu.profie,Sidemenu.bookings,Sidemenu.settings,Sidemenu.ContactUs]
        let userdata = UserDataManager.sharedInstance.getTheUserProfileFromDB()
        self.userImageView.sd_setImage(with:URL(string:userdata.userImagePath), placeholderImage: UIImage(named:"defaultIcon"))
        NotificationCenter.default.addObserver(self, selector: #selector(profileInfo), name: Notification.Name("updateImageNotification"), object: nil)
    }
    
    
    @IBAction func aboutUsBtnAction(_ sender: Any) {
      /*  let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let contactUsVC = storyBoard.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC else{
            return
        }
        contactUsVC.webUrlLink = "http://149.129.129.47/Web/aboutus"
        contactUsVC.Tittle = "About Us"
        let navCntl = UINavigationController(rootViewController: contactUsVC)
        navCntl.navigationBar.isHidden = true
        self.revealViewController().frontViewController = navCntl
        self.revealViewController().revealToggle(animated: true) */
         CXDataService.sharedInstance.showAlert(message: "Under development", viewController: self)
    }
    
    @IBAction func privacyPolicyBtnAction(_ sender: Any) {
       /* let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let contactUsVC = storyBoard.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC else{
            return
        }
        contactUsVC.webUrlLink = "http://149.129.129.47/Web/aboutus"
        contactUsVC.Tittle = "Privacy Policy"
        let navCntl = UINavigationController(rootViewController: contactUsVC)
        navCntl.navigationBar.isHidden = true
        self.revealViewController().frontViewController = navCntl
        self.revealViewController().revealToggle(animated: true) */
         CXDataService.sharedInstance.showAlert(message: "Under development", viewController: self)
    }
    
    
    @IBAction func rateUsBtnAct(_ sender: Any) {
        /* let ratingVC = storyboard?.instantiateViewController(withIdentifier: "RatingViewController")as? RatingViewController
         let navCntrl = UINavigationController(rootViewController: ratingVC!)
         self.revealViewController().frontViewController = navCntrl
         self.revealViewController().revealToggle(animated: true) */
        
    }
    
    @IBAction func inviteYourFrdBtnAct(_ sender: Any) {
        /*   let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
         let referralVC = storyBoard.instantiateViewController(withIdentifier: "InviteFriendsToShareVC") as! InviteFriendsToShareVC
         referralVC.modalPresentationStyle = .overCurrentContext
         referralVC.modalTransitionStyle = .crossDissolve
         let popover = referralVC.popoverPresentationController
         popover?.permittedArrowDirections = .any
         self.present(referralVC, animated: true, completion: nil) */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        profileInfo()
    }
    
    @objc func profileInfo(){
        let relamInstance = try! Realm()
        let userData = relamInstance.objects(UserAccountDB.self).first
        self.userName.text = userData?.Name
        self.userImageView.sd_setImage(with:  URL(string: userData?.userImagePath ?? ""), placeholderImage: UIImage(named: "defaultIcon"))
    }
    
    func tableDelegates (){
        self.tableview.dataSource = self
        self.tableview.delegate = self
    }
    
    func showAlertView(message:String, status:Int) {
        let alert = UIAlertController(title:AFWrapper.applicationName, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if status == 1 {
                AFWrapper.appDelegate?.logout()
            }
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if status == 1 {
                
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func editProfile(_ sender: Any) {
        
        //        let profileView = storyboard?.instantiateViewController(withIdentifier: "ProfileViewControllerID")as? ProfileViewController
        //        let navCntrl = UINavigationController(rootViewController: profileView!)
        //        self.revealViewController().frontViewController = navCntrl
        //        self.revealViewController().revealToggle(animated: true)
        
    }
    
    @IBAction func logoutBtnAct(_ sender: Any) {
        showAlertView(message: " Are you sure want to Logout?", status: 1)
    }
}

extension SideMenuViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topTittle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        tableView.separatorStyle = .none
        tableView.separatorColor = UIColor.clear
        cell.textLabel?.text = topTittle[indexPath.row].rawValue
        cell.imageView?.image = UIImage.init(named: tittleImgs[indexPath.row].rawValue)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        switch indexPath.row {
        case SidemenuSelection.home.rawValue:
                let HomeView = storyBoard.instantiateViewController(withIdentifier: "DashBoardViewController")as? DashBoardViewController
                let navCntrl = UINavigationController(rootViewController: HomeView!)
                self.revealViewController().frontViewController = navCntrl
                self.revealViewController().revealToggle(animated: true)
                break
            
        case SidemenuSelection.bookings.rawValue:
          
            break
            
        case SidemenuSelection.ContactUs.rawValue:
           /* let contactUsVC = storyBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            contactUsVC.webUrlLink = "http://149.129.129.47/Web/contactus"
            contactUsVC.Tittle = "Contact Us"
            let navCntl = UINavigationController(rootViewController: contactUsVC)
            navCntl.navigationBar.isHidden = true
            self.revealViewController().frontViewController = navCntl
            self.revealViewController().revealToggle(animated: true) */
             CXDataService.sharedInstance.showAlert(message: "Under development", viewController: self)
            break
        case SidemenuSelection.profie.rawValue:
            let profileView = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController")as? ProfileViewController
            let navCntrl = UINavigationController(rootViewController: profileView!)
            self.revealViewController().frontViewController = navCntrl
            self.revealViewController().revealToggle(animated: true)
            break
            
        case SidemenuSelection.settings.rawValue:
            /* let ordersView = storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
             let navCntl = UINavigationController(rootViewController: ordersView)
             self.revealViewController().frontViewController = navCntl
             self.revealViewController().revealToggle(animated: true)*/
            CXDataService.sharedInstance.showAlert(message: "Under development", viewController: self)
            break
            
        default:
            CXLog.print("Nothing")
        }
    }
}


