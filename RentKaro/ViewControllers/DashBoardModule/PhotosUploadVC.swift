//
//  PhotosUploadVC.swift
//  RentKaro
//
//  Created by Teja's Macbook on 16/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices

class PhotosUploadVC: UIViewController {

    @IBOutlet weak var headerNameLbl: UILabel!
    @IBOutlet weak var driverImgLbl: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var driverNumberLbl: UILabel!
    @IBOutlet weak var departureTimeLbl: UILabel!
    @IBOutlet weak var arrivalTimeLbl: UILabel!
    @IBOutlet weak var meterReadingLbl: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    private let spacing:CGFloat = 20.0
    var imagesArray = [[String:Any]]()
    var selectedIndex:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func registerCell() {
        photosCollectionView.register(UINib(nibName: "PhotoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "photoIdentifier")
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.photosCollectionView?.collectionViewLayout = layout
    }
    @IBAction func backNavigationAction(_ sender: Any) {
    }
    @IBAction func callButtonAction(_ sender: Any) {
    }
    @IBAction func confirmPickUpBtnAction(_ sender: Any) {
    }
}
extension PhotosUploadVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 20
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        if let collection = self.photosCollectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            self.collectionViewHeightConstraint.constant = 2*width + 60
            return CGSize(width: width, height: width)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoIdentifier", for: indexPath) as! PhotoCollectionCell
        if !imagesArray.isEmpty {
            let filtered = imagesArray.filter {$0["Index"] as? Int == selectedIndex }
            let tempDict = filtered.first
            photoCell.uploadedImageView.image = tempDict?["Image"] as? UIImage
            photoCell.uploadedImageView.contentMode = .scaleAspectFit
        }
        return photoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        showSimpleActionSheet()
    }
}

extension PhotosUploadVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func showSimpleActionSheet() {
        let alert = UIAlertController(title: "RentKaro", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.cameraOrVideo(isCamera: true)
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.cameraOrVideo(isCamera: false)
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func cameraOrVideo(isCamera: Bool) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.mediaTypes = [kUTTypeImage as String]
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            if isCamera == true {
                picker.sourceType = .photoLibrary
            } else {
                picker.sourceType = .camera;
            }
            present(picker, animated: true, completion: nil)
        } else {
           self.showAlert(withTitle: "RentKaro", andMessage: "This device doesn't had camera")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
           print(image)
        } else {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                guard let imageData = image.jpegData(compressionQuality: 0.25) else {
                    return
                }
                print(imageData)
                imagesArray.append(["Index":selectedIndex ?? 0,"Image":image,"ImageData":imageData])
                photosCollectionView.reloadItems(at: [IndexPath(item: selectedIndex ?? 0, section: 0)])
            }
        }
        self.dismiss(animated: true, completion: nil)
    }

}
