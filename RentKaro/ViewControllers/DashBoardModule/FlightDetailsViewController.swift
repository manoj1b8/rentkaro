//
//  FlightDetailsViewController.swift
//  RentKaro
//
//  Created by Apple on 13/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FlightDetailsViewController: UIViewController {

    @IBOutlet weak var departureChaufferBtn: UIButton!
    @IBOutlet weak var departureShuttleBtn: UIButton!
    @IBOutlet weak var flightNumberTF: CXTextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var mobileNoTF: UITextField!
    @IBOutlet weak var arrivalChaufferBtn: UIButton!
    @IBOutlet weak var arrivalShuttleBtn: UIButton!
    @IBOutlet weak var departureDateTF: UITextField!
    @IBOutlet weak var departureTimeTF: UITextField!
    @IBOutlet weak var arrivalDateTF: UITextField!
    @IBOutlet weak var arrivalTimeTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.departureChaufferBtn.isSelected = true
        self.arrivalChaufferBtn.isSelected = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func validation()-> Bool{
        if !(self.departureChaufferBtn.isSelected || self.departureShuttleBtn.isSelected){
            return false
        }else if (self.flightNumberTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter your flight number")
            return false
        }else if(self.nameTF.text?.isEmpty ?? true){
             self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter your name")
            return false
        }else if(self.mobileNoTF.text?.isEmpty ?? true){
             self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter mobile number")
            return false
        }else if (self.mobileNoTF.text?.count ?? 0 < 10){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please enter valid mobile number")
            return false
        }else if(self.departureDateTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select departure date")
            return false
        }else if(self.departureTimeTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select departure time")
            return false
        }else if !(self.arrivalChaufferBtn.isSelected || self.arrivalShuttleBtn.isSelected){
            return false
        }else if(self.departureDateTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select arrival date")
            return false
        }else if(self.departureTimeTF.text?.isEmpty ?? true){
            self.showAlert(withTitle: AFWrapper.applicationName, andMessage: "Please select arrival time")
            return false
        }else{
            return true
        }
    }
    
    @IBAction func departureToggleBtnAction(_ sender: UIButton) {
        if sender == self.departureChaufferBtn{
            sender.isSelected = true
            self.departureShuttleBtn.isSelected = false
        }else{
            self.departureChaufferBtn.isSelected = false
            self.departureShuttleBtn.isSelected = true
        }
    }
    
    @IBAction func arrivalToggleBtnAction(_ sender: UIButton) {
        if sender == self.arrivalChaufferBtn{
            sender.isSelected = true
            self.arrivalShuttleBtn.isSelected = false
        }else{
            self.arrivalChaufferBtn.isSelected = false
            self.arrivalShuttleBtn.isSelected = true
        }
    }
    
    @IBAction func bookNowBtnaction(_ sender: UIButton) {
        //if self.validation(){
            guard let bookingDetailsVC = storyboard?.instantiateViewController(withIdentifier: "BookingDetailsViewController") as? BookingDetailsViewController else{
                return
            }
            self.navigationController?.pushViewController(bookingDetailsVC, animated: true)
       // }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectDateBtnAction(_ sender: UIButton) {
        CXDataService.sharedInstance.selectDatePicker(completionPicking: { (selectedTimeStr) in
             if sender.tag == 1{
                self.departureDateTF.text = selectedTimeStr
             }else{
                self.arrivalDateTF.text = selectedTimeStr
            }
        }, controller: self)
    }
    
    
    @IBAction func selectTimeBtnAction(_ sender: UIButton) {
        CXDataService.sharedInstance.selectTimePicker(completionPicking: { (selectedStr) in
            if sender.tag == 1{
                self.departureTimeTF.text = selectedStr
            }else{
                self.arrivalTimeTF.text = selectedStr
            }
            
        }, controller: self)
    }
    
}

extension FlightDetailsViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location > 9{
            textField.resignFirstResponder()
            return false
        }
        return true
    }
}
