//
//  WelcomeScreenVC.swift
//  ESahai
//
//  Created by apple on 25/05/18.
//  Copyright © 2018 Rama kuppa. All rights reserved.
//

import UIKit


private typealias ScrollView = WelcomeScreenVC

class WelcomeScreenVC: UIViewController,UIScrollViewDelegate {
    @IBOutlet weak var startBtn: CXButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageView: UIPageControl!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var imageTypeLbl: UILabel!
    @IBOutlet weak var UICollectionView: UICollectionView!
    @IBOutlet weak var hiddenImage: UIImageView!
    var splashImageArr = [String]()

    override func viewDidLoad() {
        self.imageTypeLbl.text = "Please enable GPS to continue using app"
        super.viewDidLoad()
        self.splashImageArr = ["welcomeSlide1","welcomeSlide2"]
        
       // self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.scrollView.frame.height)
       /* let scrollViewWidth:CGFloat = self.scrollView.frame.width
        
        let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:scrollViewWidth, height:640))
        imgOne.image = UIImage(named: "welcomeSlide1")
        imgOne.contentMode = .scaleAspectFit
        imgOne.backgroundColor = UIColor.clear
        
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:640))
        imgTwo.image = UIImage(named: "welcomeSlide2")
        imgTwo.contentMode = .scaleAspectFit
        imgTwo.backgroundColor = UIColor.clear
        
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:640))
        imgThree.image = UIImage(named: "welcomeSlide3")
        imgThree.contentMode = .scaleAspectFit
        imgThree.backgroundColor = UIColor.clear

        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)

        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 3, height:self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageView.currentPage = 0*/
        
        NotificationCenter.default.addObserver(self, selector: #selector(postedJobSuccess), name: Notification.Name("PostedJobs"), object: nil)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        //        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        //        appDelegate?.setUpSidePanel()
        self.loginBtnAction()
    }
    
    @objc func loginBtnAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainLoginRegVC") as! MainLoginRegVC
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func postedJobSuccess(){
        self.dismiss(animated: true, completion: nil)
    }
}
extension WelcomeScreenVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.splashImageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let splashCell = collectionView.dequeueReusableCell(withReuseIdentifier: "splashCell", for: indexPath) as? SplashCollectionViewCell else {
            return UICollectionViewCell()
        }
        splashCell.splashImageView.image = UIImage(named: splashImageArr[indexPath.item])
        return splashCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageView.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.UICollectionView.frame.size.width, height: self.UICollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}

extension ScrollView {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageView.currentPage = Int(currentPage);
        if Int(currentPage) == 0 {
            self.imageTypeLbl.text = "Get a smarter way to park your vehicle quick and easy"
        }
        else if Int(currentPage) == 1 {
            self.imageTypeLbl.text = "Let’s manage the hassle of parking at airport without delay"
        }
    }
}

