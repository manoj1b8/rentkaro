//
//  CustomCollectionViewCell.swift
//  Loaders Dozers
//
//  Created by Raghavendra on 24/10/18.
//  Copyright © 2018 ongoFramework. All rights reserved.
//

import Foundation
import UIKit

class SplashCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    //MARK:- IBOutlets
    @IBOutlet weak var splashImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var firstlabel: UILabel!
    @IBOutlet weak var secondlabel: UILabel!
    
    @IBOutlet weak var thirdLabel: UILabel!
    
}
