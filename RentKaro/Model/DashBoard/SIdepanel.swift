//
//  SIdepanel.swift
//  Lifeline
//
//  Created by Rama Kuppa on 7/9/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

enum Sidemenu:String {
    case home = "Home"
    case profie = "Profile"
    case bookings = "Bookings"
    case settings = "Settings"
    case ContactUs = "Contact Us"
    
}
//Wallet
enum SidemenuSelection:Int {
    case home = 0,profie,bookings,settings,ContactUs
}
