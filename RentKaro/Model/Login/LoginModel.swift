//
//  LoginModel.swift
//  RentKaro
//
//  Created by apple on 16/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

// MARK: - OTP Model
struct OTPModel: Codable {
    let status, msg, OTP: String?
}
// MARK: - Register Model
struct RegisterModel: Codable {
    let status, msg: String?
}

