//
//  UserAccountDB.swift
//  iOSOngoTemplate
//
//  Created by AppStore MAC on 11/03/19.
//  Copyright © 2019 ongoframework. All rights reserved.
//

import Foundation
import RealmSwift

class UserAccountDB: Object{
    @objc dynamic var Email = ""
    @objc dynamic var Name = ""
    @objc dynamic var userId = ""
    @objc dynamic var firstName = ""
    @objc dynamic var fullName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var mobileNo = ""
    @objc dynamic var mobile = ""
    @objc dynamic var organisation = ""
    @objc dynamic var userImagePath = ""
    @objc dynamic var macIdJobId = ""
    @objc dynamic var macId = ""
    
    @objc dynamic var Latitude = ""
    @objc dynamic var Longitude = ""
    @objc dynamic var address = ""
    @objc dynamic var gender = ""
    @objc dynamic var isAgent = ""
    @objc dynamic var ItemCode = ""
  
    @objc dynamic var id = Int()
    @objc dynamic var state = ""
    @objc dynamic var city = ""
    @objc dynamic var area = ""
    @objc dynamic var pincode = ""
    @objc dynamic var isProfileUpdated = ""
  
}
