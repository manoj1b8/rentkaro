//
//  UserDataManager.swift
//  iOSOngoTemplate
//
//  Created by AppStore MAC on 11/03/19.
//  Copyright © 2019 ongoframework. All rights reserved.
//

import Foundation
import RealmSwift

class UserDataManager:NSObject{
    static let sharedInstance = UserDataManager()
    // Good to use private keyword to prevent other objects from creating their own instances
    private override init() {}
    
    func saveTheUserDetails(userDataDic:[String:AnyObject],completion:@escaping (_ successBool:Bool) -> Void){
        // IMMyProfile
        let relamInstance = try! Realm()
        CXLog.print(userDataDic)
        // Query Realm for userProfile contains name
        let userData = relamInstance.objects(UserAccountDB.self).filter("userId=='\(CXAppConfig.resultString(input: userDataDic["userId"] as AnyObject))'")
        if userData.count == 0 {
            //Insert The Data
            try! relamInstance.write({
                CXLog.print(userDataDic)
                let myProfileData = UserAccountDB()
                relamInstance.add(self.saveUSerData(userDataDic: userDataDic , myProfileData: myProfileData))
                // self.sendTheDeviceTokenToFirebase()
                completion(true)
            })
        }else{
            //Update the user profile
            try! relamInstance.write({
                var profile = userData.first
                profile = self.saveUSerData(userDataDic: userDataDic  , myProfileData: profile!)
                if (profile != nil){
                    completion(true)
                }
            })
        }
    }
    
    func saveUSerData(userDataDic:[String:AnyObject],myProfileData:UserAccountDB) -> UserAccountDB {
        
        if let userId = userDataDic["userId"] as? Int {
            myProfileData.userId = "\(userId)"
        }else{
            myProfileData.userId = ""
        }
        if let email = userDataDic["Email"] as? String {
            myProfileData.Email = email
        }else{
            myProfileData.Email = ""
        }
        if let firstName = userDataDic["firstName"] as? String {
            myProfileData.firstName = firstName
        }else{
            myProfileData.firstName = ""
        }
        if let lastName = userDataDic["lastName"] as? String {
            myProfileData.lastName = lastName
        }else{
            myProfileData.lastName = ""
        }
        if let fullName = userDataDic["fullName"] as? String {
            myProfileData.fullName = fullName
        }else{
            myProfileData.fullName = ""
        }
        if let macIdJobId = userDataDic["macIdJobId"] as? String {
            myProfileData.macIdJobId = macIdJobId
        }else{
            myProfileData.macIdJobId = ""
        }
        if let macId = userDataDic["macId"] as? String {
            myProfileData.macId = macId
        }else{
            myProfileData.macId = ""
        }
        if let organisation = userDataDic["organisation"] as? String {
            myProfileData.organisation = organisation
        }else{
            myProfileData.organisation = ""
        }
        if let address = userDataDic["address"] as? String {
            myProfileData.address = address
        }else{
            myProfileData.address = ""
        }
        if let image = userDataDic["userImagePath"] as? String {
            myProfileData.userImagePath = image
        }else{
            myProfileData.userImagePath = ""
        }
        if let mobileNo = userDataDic["mobileNo"] as? String {
            myProfileData.mobileNo = mobileNo
        }else{
            myProfileData.mobileNo = ""
        }
        if let gender = userDataDic["gender"] as? String {
            myProfileData.gender = gender
        }else{
            myProfileData.gender = ""
        }
        if let macId = userDataDic["ItemCode"] as? String {
            myProfileData.ItemCode = macId
        }else{
            myProfileData.ItemCode = ""
        }
        if let macIdJobId = userDataDic["id"] as? Int {
            myProfileData.id = macIdJobId
        }else{
            myProfileData.id = 0
        }
        if let name = userDataDic["Name"] as? String {
            myProfileData.Name = name
        }else{
            myProfileData.Name = ""
        }
        if let state = userDataDic["state"] as? String {
            myProfileData.state = state
        }else{
            myProfileData.state = ""
        }
        if let city = userDataDic["city"] as? String {
            myProfileData.city = city
        }else{
            myProfileData.city = ""
        }
        if let area = userDataDic["area"] as? String {
            myProfileData.area = area
        }else{
            myProfileData.area = ""
        }
        if let pincode = userDataDic["pincode"] as? String {
            myProfileData.pincode = pincode
        }else{
            myProfileData.pincode = ""
        }
        if let Latitude = userDataDic["Latitude"] as? String {
            myProfileData.Latitude = Latitude
        }else{
            myProfileData.Latitude = ""
        }
        if let Longitude = userDataDic["Longitude"] as? String {
            myProfileData.Longitude = Longitude
        }else{
            myProfileData.Longitude = ""
        }
        if let isProfileUpdated = userDataDic["isProfileUpdated"] as? String {
            myProfileData.isProfileUpdated = isProfileUpdated
        }else{
            myProfileData.isProfileUpdated = "No"
        }
        
        return myProfileData
    }
    
    func getTheUserProfileFromDB() -> UserAccountDB{
        let realm = try! Realm()
        let profileList = realm.objects(UserAccountDB.self)
        if profileList.count != 0 {
            let profile = realm.objects(UserAccountDB.self).first
            return profile!
        }else{
            return UserAccountDB()
        }
    }
    
    func isUserLoggedIn()-> Bool{
        let realm = try! Realm()
        let profile = realm.objects(UserAccountDB.self)
        if profile.count == 0 {
            return false
        }else{
            return true
        }
    }
}
